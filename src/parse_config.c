/*!
 * \file parse_config.c
 * 
 * Analyseur de fichier de configuration
 * 
 * 
 * Cette librairie sert à découper, à stocker les éléments utiles d'un fichier
 * de configuration, et à retrouver à la demande la valeur particulière d'un
 * paramètre.
 * 
 * Les actions suivantes sont conduites:
 * 
 *	- Suppression des commentaires
 *	- Assemblage du fichier en sections
 *	- Nombre quelconque de paramètres sous forme de listes
 *	- Recherche des informations par section et par mot-clef
 *
 * Le format du fichier de configuration est le suivant:
 *
 *	- Commentaires introduits par un dièse et s'étendant
 * jusqu'à la fin de ligne
 *	- Section indiquées par un mot-clef placé entre crochets. Une section
 * prend fin au début de la section suivante. Plusieurs sections de même nom
 * peuvent exister dans le fichier de configuration.
 *	- Ligne de configuration introduite par un mot-clé nommant le paramètre
 * suivi éventuellement d'espaces et du caractère égal (=). Ce mot-clé doit être
 * unique dans une section. Si il en existe plusieurs, seul le premier est pris
 * en compte.
 *	- La ou les valeurs suivent le signe = jusqu'à la fin de la ligne ou au
 * début d'un commentaire.
 *	- Les valeurs comportant des espaces sont placés entre guillements
 * ou entre apostrophes.
 *	- Les listes de valeurs sont séparées soit par des espaces, soit par
 * des virgules.
 *
 * La taille des noms de section, de paramètres et de chacune des valeurs est
 * limitée à 128 caractères.
 *
 * \verbatim
 * Exemple:
 *
 * [section1]
 *	param1	= 	valeur1
 *	param2	=	valeur2	, valeur3	# Paramètre nº 2
 * #
 * # Seconde section
 * #
 * [section2]
 *	param1	=	"chaine de caractère" 1234	# Deux valeurs
 *	param3 = "toto"
 * #
 * # Encore une seconde section
 * #
 * [section2]
 *	param1	=	"chaine de caractère" 1234	# Deux valeurs
 *	param2	=	valeur5	valeur6			# Paramètre nº 2
 *	param3 = "toto"
 *
 * \endverbatim
 *
 *
 * @(#)  $Id$  ECOC
 *
 * \author ECOCHARD Jean-Paul <jp.ecoc@free.fr>
 * \date   jeu fév 27 20:42:17 CET 2003
 */
#if defined( zos ) || defined (_TS_GCOS8_NS) || defined (_TS_GCOS8_SS)
#define _XOPEN_SOURCE_EXTENDED 1
#	include "pconfigP.h"
#	include "pconfig.h"
#else
#	include "parse_configP.h"
#	include "parse_config.h"
#endif
/*
 * Toujours les mêmes problèmes avec cette merde.
 */
#if defined (_TS_GCOS8_NS) || defined (_TS_GCOS8_SS)
#	define str_dup(s1) strsave((s1))
#else
#	define str_dup(s1) strdup((s1))
#endif

#ifdef HAVE_MMAP
/*!
 * \brief	Fermeture du fichier ouvert via MY_OPEN.
 *
 *	Cette fonction effectue la fermeture du fichier ouvert par
 *	MY_OPEN().
 *
 * \param Pointeur sur le gestionnaire de fichiers.
 *
 * \return Toujours 0.
*/
static int MY_CLOSE( MY_FILE_t *fi )
{
	if( fi == NULL ) return 0;

	if( fi->address != MAP_FAILED ) munmap( fi->address,  fi->sb.st_size );
	if( fi->fildes ) close( fi->fildes );
	free( fi );
	fi = NULL;

	return 0;
}
/*!
 * \brief	Ouverture d'un fichier via mmap.
 *
 *	Simule l'ouverture d'un fichier via mmap.
 *	Seule l'ouverture en lecture est possible.
 *	Seule la lecture via MY_GETC est possible.
 *	La fermeture du fichier s'effectue par MY_CLOSE.
 *
 * \param file	Nom du fichier à ouvrir.
 * \param perm	Permission identique à fopen().
 *
 * \return Un pointeur sur un gestionnaire de fichier ou NULL.
*/
static MY_FILE_t *MY_OPEN( char *file, char *perm )
{
	MY_FILE_t *fi = malloc( sizeof(MY_FILE_t));
	if( fi == NULL ) {
		return NULL;
	}
	memset( fi, '\0', sizeof(MY_FILE_t) );
	fi->address = MAP_FAILED;
	fi->pc = NULL;
	fi->fin = NULL;
    fi->fildes = open( file, O_RDONLY );
    if( fi->fildes < 0 ) {
		free( fi );
		return NULL;
    }
    if( fstat( fi->fildes, &fi->sb ) < 0 ) {
		free( fi );
        return NULL;
    }
    fi->address = mmap( 0, fi->sb.st_size,
			PROT_READ, MAP_PRIVATE, fi->fildes, 0 );
    if( fi->address == MAP_FAILED ) {
		free( fi );
        return NULL;
    }
	fi->pc = fi->address;
	fi->fin = fi->pc + fi->sb.st_size;
	return fi;
}

static char MY_GETC( MY_FILE_t *fi )
{
	if( fi->pc >= fi->fin ) return EOF;
	return *fi->pc++;
}
#else
#define MY_OPEN(file, perm) fopen(file, perm)
#define MY_CLOSE(file)		fclose(file)
#define MY_GETC(c)			fgetc(c)
#endif
/*!
 * \brief Analyse et découpe le fichier de configuration
 * 
 * Cette fonction effectue l'analyse complète du fichier de configuration et
 * rend un pointeur sur une structure CONF opaque.
 *
 * Des fonctions d'accès permettent d'effectuer des requêtes pour retrouver
 * sur demande les parties indispensables de la configuration.
 * 
 * Les erreurs d'analyse sont envoyées soit à une fonction de traitement
 * fournie par l'appelant, soit imprimées sur stderr, en fonction de la valeur
 * du second paramètre.
 * 
 * \param  conf_file Nom du fichier de configuration à analyser
 * \param  log_err Fonction de traitement des erreurs d'analyse.
 * 
 * \return Un pointeur sur la configuration lue ou NULL en cas d'erreurs.
*/
DLLAPI CONF	DLLCALL *parse_configuration( char *conf_file, ERR_FONCT log_err )
{
	MY_FILE_t	*fin;
	CONF		*temp;
	SECTION		*section_courante;
	PARAM		*param_courant;
	char		 buf_err[1024];
	int 		 c;

#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "Enter parse_configuration(%s)\n", conf_file );
#endif
	if( (fin = MY_OPEN( conf_file, "r" )) == NULL ) {
#if defined( unix )
		snprintf( buf_err, sizeof(buf_err),
				"[parse_configuration] %s %s", conf_file,
				strerror( errno ) );
#else
		sprintf( buf_err, "[parse_configuration] %s %s", conf_file,
				strerror( errno ) );
#endif

		PRINT_ERR( buf_err );

		return (CONF *)NULL;
	}
	temp = malloc( sizeof(CONF) );
	if( temp == NULL ) {
		MY_CLOSE( fin );
		return NULL;
	}
	temp->type = CONF_TYPE;
	temp->sections = NULL;
	temp->nb_sections = 0;
	NbErr = 0;
	while( (c = MY_GETC( fin )) != EOF ) {
		switch( c ) {
			case '#':	/* Commentaire */
				EAT_TO_EOL();
				break;
			case '\n':
				NbLignes++;
				break;
			case '\t':
			case ' ':	/* Espace sans intérêt: eat it */
				break;
			case '[':	/* Titre de section */
				section_courante = read_section( fin, temp, log_err );
				if( !section_courante ) {
					detruire_configuration( temp );
					MY_CLOSE( fin );
					return NULL;
				}
				break;
			default:		/* Paramètre */
				param_courant = read_param( fin, c, section_courante, log_err );
				if( !param_courant ) {
					detruire_configuration( temp );
					MY_CLOSE( fin );
					return NULL;
				}
				break;
		}
	}

	MY_CLOSE( fin );

	if( NbErr ) {
		detruire_configuration( temp );
		temp = NULL;
	}

	return temp;
}
/*!
 * \brief Lecture du nom d'une section
 *
 * Lit le nom de la section placé entre crochets "[section]". Retourne une
 * nouvelle section pouvant stocker des paramètres.
 *
 * \param fin	Pointeur sur le fichier de configuration à lire.
 * \param cfg	Pointeur sur le stockage de la configuration.
 * \param log_err Fonction de traitement d'erreurs ou NULL.
 *
 * \return Un pointeur sur la section courante ou NULL.
 */
static SECTION	*read_section( MY_FILE_t *fin,
		CONF *cfg, ERR_FONCT log_err )
{
	SECTION	*tmp = NULL;
	char	section_name[SECTION_NAME_LEN];
	int	pos = 0;
	int	c;

#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "Enter read_section()\n" );
#endif
	while( (c = MY_GETC( fin )) != EOF ) {
		switch( c ) {
			case ']':		/* OK Section */
				c = cfg->nb_sections;
				tmp = realloc( cfg->sections,
						(c+1)*sizeof(SECTION) );
				if( tmp == NULL ) {
					NbErr++;
					PRINT_ERR( "Creation d'une section "
							"impossible" );
					return NULL;
				}
				cfg->sections = tmp;
				tmp = cfg->sections + c;
				cfg->nb_sections++;

				section_name[pos] = '\0';
#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
				DPRINT( "%d [%s]\n", NbLignes, section_name );
#endif
				tmp->nom = str_dup( section_name );
				tmp->nb_params = 0;
				tmp->params = NULL;

				return tmp;
			case ' ':
			case '\t':		/* Space eat it */
				break;
			case '\n':
				NbErr++;
				PRINT_ERR( "Saut de ligne dans un nom "
						"de section");
				NbLignes++;
				return NULL;
			default:
				section_name[pos++] = c;
				if( pos >= sizeof( section_name ) ) {
					NbErr++;
					PRINT_ERR( "Nom de section trop long" );
					EAT_TO_EOL();

					return NULL;
				}
				break;
		}
	}
	NbErr++;
	PRINT_ERR( "Fin de fichier rencontree dans un nom de section" );

	return NULL;
}
/*!
 * \brief Lecture d'un paramètre.
 *
 * Lit le nom d'un paramètre, crée le paramètre correspondant à ce nom et
 * stocke la totalité des valeurs associées à ce nom.
 *
 * \param fin	Pointeur sur le fichier de configuration à lire.
 * \param car	Caractère lu dans le fichier de configuration par l'appelant.
 * \param sec	Pointeur sur le stockage de la section.
 * \param log_err Fonction de traitement d'erreurs ou NULL. 
 *
 * \return Un pointeur sur le paramètre courant ou NULL.
*/
static PARAM	*read_param( MY_FILE_t *fin, int car,
		SECTION *sec, ERR_FONCT log_err )
{
	PARAM	*tmp = NULL;
	char	param_name[PARAM_NAME_LEN];
	int	pos = 0;
	int	id;
	int	c;

#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "Enter read_param()\n" );
#endif
	if( car == '=' ) {
		NbErr++;
		PRINT_ERR( "= au lieu de nom de parametre" );

		return NULL;
	}
	param_name[pos++] = car;
	while( (c = MY_GETC( fin )) != EOF ) {
		switch( c ) {
			case ' ':		/* OK ParamName */
			case '\t':
			case '=':
				id = sec->nb_params;
				tmp = realloc( sec->params, (id+1)*sizeof(PARAM) );
				if( tmp == NULL ) {
					NbErr++;
					PRINT_ERR( "Creation d'un parametre impossible" );
					return NULL;
				}
				sec->params = tmp;
				tmp = sec->params + id;
				tmp->nb_valeurs = 0;
				tmp->valeurs = NULL;
				
#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
				DPRINT( "\t\t%.*s = \t", pos, param_name );
#endif
				if( !read_valeurs( fin, c, tmp, log_err ) ) {
					free( tmp );
					return NULL;
				} else {
					sec->nb_params++;
				}

				param_name[pos] = '\0';
				tmp->nom = str_dup( param_name );

				return tmp;
			case '\n':
				NbErr++;
				PRINT_ERR( "Saut de ligne dans un nom "
						"de parametre");
				NbLignes++;
				return NULL;
			case '#':
				NbErr++;
				PRINT_ERR( "Commentaire dans un nom "
						"de parametre." );
				EAT_TO_EOL();
				return NULL;
			default:
				param_name[pos++] = c;
				if( pos >= sizeof( param_name ) ) {
					NbErr++;
					PRINT_ERR("Nom de parametre trop long");
					EAT_TO_EOL();

					return NULL;
				}
				break;
		}
	}
	NbErr++;
	PRINT_ERR( "Fin de fichier rencontree dans un nom de parametre" );

	return NULL;
}
/*!
 * \brief Lire une chaine de caractère
 * 
 *
 * \param fin	Pointeur sur le fichier de configuration à lire.
 * \param car	Caractère lu dans le fichier de configuration par l'appelant.
 * \param pars	Pointeur sur la structure de stockage d'un paramètre.
 * \param log_err Fonction de traitement d'erreurs ou NULL. 
 *
 * \return Un pointeur sur la chaîne courante ou NULL.
 */
static char *read_string( MY_FILE_t *fin, int car,
		PARAM *pars, ERR_FONCT log_err )
{
	static char	param_val[VALEUR_LEN];
	int		pos = 0;
	int		c;
#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "Enter read_string()\n" );
#endif
	while( (c = MY_GETC( fin )) != EOF ) {
		if( c == car ) {	/* Fin de chaine */
			param_val[pos] = '\0';
			return param_val;
		}
		if( c == '\\' ) {
			c = MY_GETC( fin );
			if( c != EOF ) {
				param_val[pos++] = c;
				if( pos >= sizeof( param_val ) ) {
					NbErr++;
					PRINT_ERR( "Valeur de parametre "
							"trop longue" );
					EAT_TO_EOL();

					return NULL;
				}
				if( c == '\n' ) {
					NbLignes++;
				}
				continue;
			}
			NbErr++;
			PRINT_ERR( "Fin de fichier inattendu" );
			return NULL;
		}
		if( c == '\'' || c == '"' ) {
			NbErr++;
			PRINT_ERR( "Caractère ' ou \" non protégé "
				"dans une chaîne");
			EAT_TO_EOL();
			return NULL;
		}
		param_val[pos++] = c;
		if( pos >= sizeof( param_val ) ) {
			NbErr++;
			PRINT_ERR( "Valeur de parametre trop longue" );
			EAT_TO_EOL();

			return NULL;
		}
	}
	NbErr++;
	PRINT_ERR( "Fin de fichier rencontrée en lisant une chaîne" );
	return NULL;
}
/*!
 * \brief Lecture des valeurs d'un paramètre.
 *
 * Stocke la totalité des valeurs associées à un paramètre donné.
 *
 * \param fin	Pointeur sur le fichier de configuration à lire.
 * \param car	Caractère lu dans le fichier de configuration par l'appelant.
 * \param pars	Pointeur sur la structure de stockage d'un paramètre.
 * \param log_err Fonction de traitement d'erreurs ou NULL. 
 *
 * \return Un pointeur sur le paramètre courant ou NULL.
 */
static char	*read_valeurs( MY_FILE_t *fin, int car,
		PARAM *pars, ERR_FONCT log_err )
{
	char	param_val[VALEUR_LEN];
	char	*pt;
	int	pos = 0;
	int	inVal = 0;
	int	c;

#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "Enter read_valeurs()\n" );
#endif
	/* Aller jusqu`au signe égal */
	if( car != '=' ) {
		while( (c = MY_GETC( fin )) != EOF ) {
			if( c == '=' ) break;
			if( c == '\n' ) {
				NbErr++;
				PRINT_ERR( "Saut de ligne au lieu d'une "
						"valeur de parametre" );
				NbLignes++;
				return NULL;
			}
			if( c != ' ' && c != '\t' ) {
				NbErr++;
				PRINT_ERR( "Valeur de parametre avant '='" );
				EAT_TO_EOL();
				return NULL;
			}
		}
	}
	/* eat first spaces */
	while( (c = MY_GETC( fin )) != EOF ) {
		if( c == ' ' || c == '\t' ) continue;
		if( c == '\n' ) {
			NbErr++;
			PRINT_ERR( "Pas de valeur pour ce parametre !" );
			NbLignes++;

			return NULL;
		}
		if( c == '#' ) {
			NbErr++;
			PRINT_ERR( "Commentaire au lieu d'une valeur." );
			EAT_TO_EOL();

			return NULL;
		}
		break;
	}

	if( c == '"' || c == '\'' ) {
		pt = read_string( fin, c, pars, log_err );
		if( pt == NULL ) {
			return NULL;
		} else {
			if( !store_val( pars, pt, log_err ) ) {
				inVal = 0;
				EAT_TO_EOL();
				return NULL;
			}
		}
		inVal = 0;
	} else {
		inVal = 1;
		param_val[pos++] = c;
	}

	while( (c = MY_GETC( fin )) != EOF ) {
		switch( c ) {
			case '#':		/* Commentaire */
				EAT_TO_EOL();
				if( !inVal ) {
					return pars->valeurs[0];
				}
			case ' ':		/* Valeur lue */
			case '\t':
			case ',':
				if( !inVal ) break;
			case '\n':
				if( !inVal ) {
					pos = 0;
					NbLignes++;
					return pars->valeurs[0];
				}
				param_val[pos] = '\0';
				pos = 0;
				inVal = 0;
				if( !store_val( pars, param_val, log_err ) ) {
					inVal = 0;
					EAT_TO_EOL();
					return NULL;
				}
				if( c == '\n' ) {
					NbLignes++;
					return pars->valeurs[0];
				}
				break;
			case '"':		/* String */
			case '\'':
				pt = read_string( fin, c, pars, log_err );
				if( pt == NULL ) {
					detruire_param( pars );
					return NULL;
				} else {
					if( !store_val( pars, pt, log_err ) ) {
						inVal = 0;
						EAT_TO_EOL();
						return NULL;
					}
				}
				break;
			case '\\':		/* Protected char */
				c = MY_GETC( fin );
				if( c == EOF ) {
					NbErr++;
					PRINT_ERR("Fin de fichier inattendue");
					
					detruire_param( pars );
					return NULL;
				}
				if( c == '\n' ) {
					NbLignes++;
					break;	/* Continuation line */
				}
				inVal = 1;
				param_val[pos++] = c;
				if( pos >= sizeof( param_val ) ) {
					NbErr++;
					PRINT_ERR( "Valeur de parametre trop "
							"longue" );
					EAT_TO_EOL();

					return NULL;
				}
				break;
			default:
				inVal = 1;
				param_val[pos++] = c;
				if( pos >= sizeof( param_val ) ) {
					NbErr++;
					PRINT_ERR( "Valeur de parametre trop "
							"longue" );
					EAT_TO_EOL();

					return NULL;
				}
				break;
		}
	}
	NbErr++;
	PRINT_ERR( "Fin de fichier rencontree en lecture "
			"d'une valeur de parametre." );

	return NULL;
}
/*!
 * \brief Stocker une valeur
 *
 * Stocke le contenu d'une valeur d'un paramètre. 
 *
 * \param pars	Pointeur sur le paramètre en cours.
 * \param val	Pointeur sur la valeur à stocker.
 * \param log_err Fonction de traitement d'erreurs ou NULL. 
 *
 * \return Pointeur sur la valeur stockée ou NULL en cas de pb d'allocation.
 */
static char *store_val( PARAM *pars, char *val, ERR_FONCT log_err )
{
	char	**tmp = NULL;
	int	c;

#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "Enter store_val()\n" );
#endif
	c = pars->nb_valeurs;
	tmp = realloc( pars->valeurs, (c+1)*sizeof(char *) );
	if( tmp == NULL ) {
		NbErr++;
		PRINT_ERR( "Stockage d'une valeur impossible" );
		detruire_param( pars );

		return NULL;
	}
	pars->valeurs = tmp;
	pars->nb_valeurs++;
	tmp[c] = str_dup( val );

#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "\t\t==> %s\n", tmp[c] );
#endif

	return tmp[c];
}
/*!
 * \brief Détruire une configuration.
 * 
 * Supprime définitivement l'ensemble des valeurs de configuration stockées.
 * L'espace mémoire alloué est rendu.
 * 
 * \param cfg Pointeur sur la configuration à détruire.
 * 
 * \return void 
 */
DLLAPI void DLLCALL detruire_configuration( CONF *cfg )
{
	if( !cfg || cfg->type != CONF_TYPE ) return;

	if( cfg->nb_sections && cfg->sections ) {
		int	i;

		for( i = 0; i < cfg->nb_sections; i++ ) {
			detruire_section( cfg->sections+i );
		}
		free( cfg->sections );
		cfg->nb_sections = 0;
	}

	free( cfg );
}
/*!
 * \brief Destruction d'une section du fichier de configuration.
 * 
 * Parcourt la liste des paramètres et détruit chacun d'eux.
 *
 * \param  sec SECTION à détruire.
 *
 * \return void 
 */
static void detruire_section( SECTION *sec )
{
	if( sec->nb_params && sec->params ) {
		int	i;

		for( i = 0; i < sec->nb_params; i++ ) {
			detruire_param( sec->params+i );
		}

		free( sec->params );
		sec->nb_params = 0;
	}
	if( sec->nom ) free( sec->nom );
}
/*!
 * \brief Destruction d'un paramètre du fichier de configuration.
 * 
 * Parcourt la liste des valeurs d'un paramètre et détruit chacune d'elles.
 *
 * \param  par PARAM à détruire.
 *
 * \return void 
 */
static void detruire_param( PARAM *par )
{
	if( par->nb_valeurs && par->valeurs ) {
		int	i;

		for( i = 0; i < par->nb_valeurs; i++ ) {
			if( *(par->valeurs+i) ) free( par->valeurs[i] );
		}

		free( par->valeurs );
		par->nb_valeurs = 0;
	}
	if( par->nom ) free( par->nom );
}
/*!
 * \brief Donne de nombre de sections identiques du fichier de configuration.
 * 
 * Cette fonction retourne le nombre de section de même nom contenues dans
 * le fichier de configuration.
 *
 * \param  conf		Fichier de configuration après parsing.
 * \param section	Nom de la section à traiter.
 *
 * \return Le nombre de sections identiques (1 à n) ou -1 en cas d'erreur.
 */
DLLAPI int DLLCALL nb_sections_configuration( CONF *conf, char *section )
{
	int nb_sections_identiques;

	if( !conf || conf->type != CONF_TYPE ) return -1;

	nb_sections_identiques = 0;

	if( conf->nb_sections && conf->sections ) {
		SECTION	*pts = conf->sections;
		int	i;
		
		for( i=0; i < conf->nb_sections; i++, pts++ ) {
			if( pts->nom && !strcmp( section, pts->nom ) ) {
				nb_sections_identiques++;
			}
		}
	}

	return nb_sections_identiques;
}
/*!
 * \brief Donne le nombre de paramètres d'une section
 * 
 * Cette fonction retourne le nombre de paramètres contenus dans la "num" ième
 * section de nom "nom" du fichier de configuration.
 *
 * \param  conf		Fichier de configuration après parsing.
 * \param section	Nom de la section à traiter.
 * \param num		Numéro de la section de nom identique à traiter.
 *
 * \return Le nombre de paramètres dans la section ou -1 en cas d'erreur.
 */
DLLAPI int DLLCALL nb_parametres_configuration( CONF *conf, char *section, int num )
{
	if( !conf || conf->type != CONF_TYPE ) return -1;

	if( conf->nb_sections && conf->sections ) {
		SECTION	*pts = conf->sections;
		int	cpt = 0;
		int	i;
		
		for( i=0; i < conf->nb_sections; i++, pts++ ) {
			if( pts->nom && !strcmp( section, pts->nom ) ) {
				if( cpt == num )
					return pts->nb_params;
				else
					cpt++;
			}
		}

		return 0;
	}
	return -1;
}
/*!
 * \brief Accèder aux valeurs d'un paramètre.
 * 
 * Cette fonction permet de retrouver la valeur d'un paramètre dans une
 * section donnée.
 *
 * Si plusieurs sections de même nom existent, la section indiquée par
 * la variable "num" est choisie. La première section possède le numéro 0.
 *
 * \param conf		Pointeur sur le fichier de configuration après parsing.
 * \param section	Nom de la section à chercher.
 * \param num		Numéro de la section au cas où plusieurs sections de
 * même nom existent.
 * \param parametre	Nom du parametre cherché.
 * \param valeurs	Pointeur sur un tableau de pointeurs sur la ou
 * les chaines de caractères contenant les valeurs du paramètre ou
 * NULL en cas d'erreur. Si NULL en entrée, la foncion rend seulement 
 * le nombre de valeurs associées au paramètre.
 *
 * \return Le nombre de paramètres retrouvés ou -1 en cas d'erreur.
 */
DLLAPI int DLLCALL valeurs_configuration( CONF *conf, char *section, int num, char *parametre, char ***valeurs )
{
	SECTION	*pts;
	PARAM	*ptp;
	int	i;

	if( valeurs) *valeurs = NULL;
	
	if( !conf || conf->type != CONF_TYPE ) return -1;

	if( !conf->nb_sections || !conf->sections ) return -1;

	/* Chercher la num ième section */
	pts = conf->sections;
	ptp = NULL;

	for( i=0; i < conf->nb_sections; i++, pts++ ) {
		if( pts->nom && !strcmp( section, pts->nom ) ) {
			if( num ) {
				num--;
				continue;
			}
			ptp = pts->params;
			/* Section trouvée */
			if( !pts->nb_params && !ptp ) return -1;

			break;
		}
	}

	if( ptp == NULL ) return -1;
	if( !valeurs ) return ptp->nb_valeurs;
	/* Chercher le paramètre */
	for( i=0; i<pts->nb_params; i++, ptp++ ) {
		if( ptp->nom && !strcmp( parametre, ptp->nom ) ) {
			*valeurs = ptp->valeurs;

			return ptp->nb_valeurs;
		}
	}

	return -1;
}
/*!
 * \brief Accèder aux valeurs du nième paramètre.
 * 
 * Cette fonction permet de retrouver la valeur du nième paramètre dans une
 * section donnée.
 *
 * Si plusieurs sections de même nom existent, la section indiquée par
 * la variable "num" est choisie. La première section possède le numéro 0.
 *
 * \param conf		Pointeur sur le fichier de configuration après parsing.
 * \param section	Nom de la section à chercher.
 * \param num		Numéro de la section au cas où plusieurs sections de
 * même nom existent.
 * \param parametre	Nom du parametre cherché.
 * \param num_par	Numéro du paramètre au cas où plusieurs paramètres de
 * même nom existent.
 * \param valeurs	Pointeur sur un tableau de pointeurs sur la ou
 * les chaines de caractères contenant les valeurs du paramètre ou
 * NULL en cas d'erreur. Si NULL en entrée, la foncion rend seulement 
 * le nombre de valeurs associées au paramètre.
 *
 * \return Le nombre de paramètres retrouvés ou -1 en cas d'erreur.
 */
DLLAPI int DLLCALL valeurs_configuration_n( CONF *conf, char *section, int num, char *parametre, int num_par, char ***valeurs )
{
	SECTION	*pts;
	PARAM	*ptp;
	int	i;

	if( valeurs) *valeurs = NULL;
	
	if( !conf || conf->type != CONF_TYPE ) return -1;

	if( !conf->nb_sections || !conf->sections ) return -1;

	/* Chercher la num ième section */
	pts = conf->sections;
	ptp = NULL;

	for( i=0; i < conf->nb_sections; i++, pts++ ) {
		if( pts->nom && !strcmp( section, pts->nom ) ) {
			if( num ) {
				num--;
				continue;
			}
			ptp = pts->params;
			/* Section trouvée */
			if( !pts->nb_params && !ptp ) return -1;

			break;
		}
	}

	if( ptp == NULL ) return -1;

	/* Chercher le paramètre */
	for( i=0; i<pts->nb_params; i++, ptp++ ) {
		if( ptp->nom && !strcmp( parametre, ptp->nom ) ) {
			if( num_par ) {
				--num_par;
				continue;
			}
			if( valeurs ) *valeurs = ptp->valeurs;

			return ptp->nb_valeurs;
		}
	}

	return -1;
}
/*!
 * \brief Accèder aux noms de toutes les sections.
 * 
 * Cette fonction permet de retrouver le nom des sections dans une
 * configuration donnée.
 *
 * Si plusieurs sections de même nom existe, le nom est rendu une seule fois.
 *
 * IMPORTANT:
 *
 * Le tableau des noms des sections rendus doit être libéré par l'appelant par
 * l'appel à detruire_liste_configuration().
 *
 * \param conf		Pointeur sur le fichier de configuration après parsing.
 * \param noms	Pointeur sur un tableau de pointeurs sur la liste
 * des noms des sections ou NULL en cas d'erreur. Si NULL en entrée, la fonction
 * rend seulement le nombre de sections.
 *
 * \return Le nombre de sections retrouvé ou -1 en cas d'erreur.
 */
DLLAPI int DLLCALL liste_sections_configuration( CONF *conf, char ***noms )
{
	SECTION	 *pts;
	char	**stock;
	int	  i, j, nb_sections_uniques;

	if( noms) *noms = NULL;

	if( !conf || conf->type != CONF_TYPE ) return -1;

	if( !conf->nb_sections || !conf->sections ) return -1;

	if( !noms ) return conf->nb_sections;

	nb_sections_uniques = 0;
	/* Parcourir la liste des sections. */
	pts = conf->sections;
	stock = calloc( 2, sizeof(char **) );
	
	for( i=0; i < conf->nb_sections; i++, pts++ ) {
		for( j=1; j<nb_sections_uniques+1 && stock[j]; j++ ) {
			if( !strcmp( pts->nom, stock[j] ) ) {
				j = -1;
				break;
			}
		}
		if( j < 0 ) continue;
		nb_sections_uniques++;
		stock = realloc( stock, (nb_sections_uniques+2) * sizeof(char **) );
		stock[nb_sections_uniques] = str_dup( pts->nom );
	}	
	*noms = stock+1;
	*stock = CONF_TYPE;
#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "Adresse de stock: %p\n", p );
#endif
	*noms = stock+1;
	*stock = CONF_TYPE;
	stock[nb_sections_uniques+1] = NULL;

	return nb_sections_uniques;
}
/*!
 * \brief Accèder aux noms de tous les paramètres d'une section.
 * 
 * Cette fonction permet de retrouver le nom des paramètres dans une
 * section donnée.
 *
 * Si plusieurs sections de même nom existe, la section dont le rang est 
 * indiquée par la variable "num" est choisie. La première section possède le numéro 0.
 *
 * IMPORTANT:
 *
 * Le tableau des noms des paramètres rendus doit être libéré par l'appelant par
 * l'appel à detruire_liste_configuration().
 *
 * \param conf		Pointeur sur le fichier de configuration après parsing.
 * \param section	Nom de la section à chercher.
 * \param num		Numéro de la section au cas où plusieurs sections de
 * même nom existent.
 * \param noms	Pointeur sur un tableau de pointeurs sur la liste
 * des noms des paramètres ou NULL en cas d'erreur. Si NULL en entrée, la fonction
 * rend seulement le nombre de paramètres associés à la section.
 *
 * \return Le nombre de paramètre retrouvé ou -1 en cas d'erreur.
 */
DLLAPI int DLLCALL liste_parametres_configuration( CONF *conf, char *section, int num, char ***noms )
{
	SECTION	 *pts;
	PARAM	 *ptp;
	char	**stock;
	int	  i;

	if( noms) *noms = NULL;
	
	if( !conf || conf->type != CONF_TYPE ) return -1;

	if( !conf->nb_sections || !conf->sections ) return -1;
	/* Chercher la num ième section */
	pts = conf->sections;
	ptp = NULL;

	for( i=0; i < conf->nb_sections; i++, pts++ ) {
		if( pts->nom && !strcmp( section, pts->nom ) ) {
			if( num ) {
				num--;
				continue;
			}
			ptp = pts->params;
			/* Section trouvée */
			if( !pts->nb_params && !ptp ) return -1;

			break;
		}
	}
	if( ptp == NULL ) return -1;
	if( !noms ) return pts->nb_params;
	/* Parcourir la liste des paramètres */
	stock = calloc( pts->nb_params+2, sizeof( char ** ) );
#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "Adresse de stock: %p\n", p );
#endif
	*noms = stock+1;
	*stock = CONF_TYPE;
	for( i=0; i<pts->nb_params; i++, ptp++ ) {
		if( ptp->nom ) {
			stock[i+1] = str_dup( ptp->nom );
		} else {
			stock[i+1] = str_dup( "" );
		}
	}
	stock[i+1] = NULL;
	return pts->nb_params;
}
/*!
 * \brief Détruire une liste de noms de paramètres ou de sections.
 * 
 * Cette fonction permet de libérer la place prise par la liste des noms de
 * paramètres pour une section donnée ou la liste de sections.
 *
 * \param noms	Pointeur sur le tableau de pointeurs sur la liste
 * des noms des paramètre obtenues par liste_parametres_configuration() ou
 * la liste de sections de liste_sections_configuration().
 *
 * \return Rien.
 */
DLLAPI void DLLCALL detruire_liste_configuration( char ***noms )
{
	int	i;

	if( noms == NULL ) return;

	if( (*noms)[-1] != CONF_TYPE ) {
		fprintf( stderr, "Oooh!!! Est-ce bien une liste de paramètres ?\n" );
		return;
	}
	for( i=0; noms && (*noms)[i]; i++ ) {
		free( (*noms)[i] );
	}
#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
	DPRINT( "Adresse de libération: %p\n", (*nom)-1 );
#endif
	free( (*noms)-1 );
}
