/*!
 * \file parse_configP.h
 * 
 * 						__      ______  ______
 * 					   /  \    / _____|| _____|
 * 					  / /\ \  | /      | |___
 * 					 / /  \ \ | |      |  ___|
 * 					/ /____\ \| |      | | 
 * 				   / /______\ \ \_____ | |____
 * 				  /_/        \_\______||______|
 *
 * \brief Entete privée de l'analyseur de fichier de configuration
 * 
 *
 * @(#)  $Id$  ECOC
 *
 * \author ECOCHARD Jean-Paul <jp.ecoc@free.fr>
 * \date   jeu fév 27 20:42:17 CET 2003
 */
#ifndef _PARSE_CONFIGP_H
#define _PARSE_CONFIGP_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#ifdef HAVE_CONFIG_H
#   include "config.h"
#endif

#ifdef HAVE_MMAP
#   include <sys/mman.h>
#	include <sys/types.h>
#	include <sys/stat.h>
#	include <unistd.h>
#	include <fcntl.h>
#endif
/*!
 *	Macro d'impression de commentaires en mode DEBUG.
 */
#if !defined (_TS_GCOS8_NS) && !defined (_TS_GCOS8_SS) && !defined( __IBMC__ )
#	ifdef DEBUG
#		define DPRINT(...)	fprintf( stderr, __VA_ARGS__)
#	else
#		define DPRINT(...)
#	endif
#endif
/*!
 * Taille maximum du nom d'une section.
 */
#define SECTION_NAME_LEN	128
/*!
 * Taille maximum du nom d'un paramètre.
 */
#define PARAM_NAME_LEN		128
/*!
 * Taille maximum du nom d'une valeur.
 */
#define VALEUR_LEN		128
/*!
 *	Structure d'un paramètre.
 */
typedef struct PARAM {
	char	*nom;		/*!< Nom du paramètre. */
	int	nb_valeurs;	/*!< Nombre de valeurs du paramètre. */
	char	**valeurs;	/*!< Tableau des valeurs du paramètre. */
} PARAM;
/*!
 *	Structure d'une section.
 */
typedef struct _SECTION_ {
	char	*nom;		/*!< Nom de la section. */
	int	nb_params;	/*!< Nombre de paramètres dans la section. */
	PARAM	*params;	/*!< Tableau des paramètres. */
} SECTION;
/*!
 *	Structure du fichier de configuration.
 */
typedef struct _CONF_ {
	char	*type;		/*!< Type de la structure conf. */
	int	nb_sections;	/*!< Nombre totale de sections. */
	SECTION	*sections;	/*!< Tableau des sections. */
} CONF;
#ifdef HAVE_MMAP
typedef struct _MY_FILE_s {
	char		*address;	/* Adresse du mmap. */
	struct stat	 sb;		/* fstat du fichier. */
	char		*fin;		/* Adresse de fin du fichier. */
	char		*pc;		/* Pointeur courant dans le fichier. */
	int			 fildes;	/* Handle du fichier */
} MY_FILE_t;
#else
typedef FILE	MY_FILE_t;
#endif
/*!
 * \brief	Type de la structure CONF.
 */
static char	*CONF_TYPE = "CONF_TYPE";
/*!
 * \brief	Comptage du numéro de ligne pour les messages d'erreurs.
 */
static int	NbLignes = 1;
/*!
 * \brief	Comptage du nombre d'erreurs dans le fichier de configuration.
 */
static int	NbErr = 0;
/*!
 * \brief	Macro de gestion d'erreur.
 *
 * Cette macro appelle la fonction utilisateur log_err si elle existe sinon
 * le message d'erreur est affiché sur STDERR.
 */
#define PRINT_ERR( a )					\
	if( log_err ) {					\
		log_err( NbLignes, (a) );		\
	} else {					\
		fprintf(stderr,"%d %s\n",NbLignes,(a));	\
	}						\
	NbErr++;
/*!
 * Cette macro mange tous les caractères jusqu'à la fin d'une ligne ou du
 * fichier.
 */
#define EAT_TO_EOL()					\
	while( (c = MY_GETC( fin )) != '\n' && c != EOF );	\
	if( c == '\n' ) NbLignes++;

/*! \brief Lecture du nom d'une section. */
static SECTION	*read_section( MY_FILE_t *, CONF *, void (*)(int, char *) );
/*! \brief Lecture d'un paramètre. */
static PARAM	*read_param( MY_FILE_t *, int, SECTION *, void (*)(int, char *) );

/*! \brief Lire une chaine de caractère. */
static char	*read_string( MY_FILE_t *, int, PARAM *, void (*)(int, char *) );
/*! \brief Lecture des valeurs d'un paramètre. */
static char	*read_valeurs( MY_FILE_t *, int, PARAM *, void (*)(int, char *) );
/*! \brief Stocker une valeur. */
static char	*store_val( PARAM *, char *, void (*)(int, char *) );
/*! \brief Destruction d'une section du fichier de configuration. */
static void	detruire_section( SECTION * );
/*! \brief Destruction d'un paramètre du fichier de configuration. */
static void	detruire_param( PARAM * );

#endif
