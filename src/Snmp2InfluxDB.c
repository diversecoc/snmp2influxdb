/*!
 * \file Snmp2InfluxDB.c
 *
 * Stockage de collectes SNMP dans une base InfluxDB.
 * Les requêtes peuvent concerner des valeurs simples (GET) ou des tables (WALK).
 *
 * - Basé sur la librairie net-snmp pour la gestion de SNMPv& à v3.
 * - Utilise le protocole InfluxDB v1 pour le stockage (c-a-d pas de token, bucket et organisation).
 * - Configuration style fichier .ini:
 * \verbatim
  		[influx]
  			Options connexion vers InfluxDB.
  		[snmp-auth]
  			Option d'authentification SNMP.
  		[mesure]
  			Options requêtes SNMP.
 *	\endverbatim
 *
 * 	\see dispatche_configuration()
 *
 * \author ECOCHARD Jean-Paul <jp.ecoc@free.fr>
 * \copyright   : Tous droits réservés
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#ifndef HOST_NAME_MAX
#include <limits.h>
#endif

#include "parse_config.h"

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

#include <influx.h>
#include <jansson.h>

struct tags {
	int nb;
	struct _tag_ {
		char*	var;
		char*	val;
	} *data;
};

typedef struct _snmp2influxdb_oid_ {
	char*	name;
	oid		oid[MAX_OID_LEN];
	size_t	oid_len;
} _snmp2influxdb_oid_t;

typedef struct _elem_ {
	enum { field, table }	type;
	char*					name;
	_snmp2influxdb_oid_t	oid;
} elem_t;

typedef struct mesure {
	char*			name;
	unsigned long*	index_filter;
	int				nb_index_filter;
	int				nb_elems;
	elem_t*			elems;
} mesure_t;

typedef struct _snmp2influxdb_config_ {
	int			version;		/* SNMP version: 1, 2c ou 3 */
	char*		peername;		/* SNMP agent hostname */
	char*		username;		/* SNMPv3 user name. */
	char*		community;		/* SNMPv1 et v2 community. */
	int			sec_level;		/* Type de sécurité SNMPv3. */
	char*		AuthPass;		/* SNMPv3 mot de passe Authentification */
	char*		CryptPass;		/* SNMPv3 mot de passe Cryptage */
	char*		AuthProto;		/* SNMPv3 protocole d'authetification. */
	char*		CryptProto;		/* SNMPv3 protocole de cryptage. */

	char*		influx_host;	/* InfluxDB host */
	char*		influx_user;	/* InfluxDB database user. */
	char*		influx_pass;	/* Password for this user. */
	char*		influx_base;	/* InfluxDB database name. */
	size_t		frequency;		/* Collecte frequency. */

	size_t		nb_oids;		/* Oid list size. */
	mesure_t*	oids;			/* Oid list to be queried. */
	struct tags tags;			/* Liste de tags globaux. */
} _snmp2influxdb_config_t;

static _snmp2influxdb_config_t	Config;
static int						Verbose = 0;
static int						TerminaisonAsked = 0;
static char						HostName[HOST_NAME_MAX+1];
#if 0
/*!
 * \n Display oid digits.
 *
 * @param oid		l'iod
 * @param oid_len	le nombre d'éléments.
 */
static void print_oid( oid* oid, size_t oid_len )
{
	for( int i=0; i<oid_len-1; i++ ) printf( "%lu.", oid[i] );
	printf( "%lu\n", oid[oid_len-1] );
}
#endif
/*!
 * \brief Création des mesures à collecter.
 *
 *	Alloue une structure mesure suffisante pour contenir 'nb' groupe de mesures.
 * Une mesure au sens InfluxDB comporte un ou plusieurs OIDs à requêter.
 *
 * @param nb	Nombre de section mesure dans le fichier configuration.
 *
 * @return	NULL ou un pointeur sur la liste de mesures.
 */
static mesure_t* new_mesure( int nb )
{
	if( nb <= 0 ) return NULL;

	mesure_t* mes = calloc( nb, sizeof(mesure_t) );

	return mes;
}
/*!
 * \brief Stocker une liste d'index pour filtrer les éléments d'une table.
 *
 * @param m			Pointeur sur la mesure
 * @param valeurs
 */
static void store_index( mesure_t* m, int nb, char** valeurs )
{
	m->nb_index_filter = nb;
	m->index_filter = calloc( nb, sizeof(char*) );

	for( int i = 0; i < nb; i++ ) {
		m->index_filter[i] = (unsigned long)atol( valeurs[i] );
	}
}
/*!
 * \brief Stocker un OID à traiter par GET.
 *
 *	Stocke le contenu d'un paramètre 'field':
 *		- field = nom, OID.
 *
 *  Le nom est un field (au sens InfluxDB) stocké dans la mesure correpondante.
 *  L'index (dernier champ) de l'oid est ajouté comme tag (au sens InfluxDB).
 *	L'OID peut être fourni sous une forme numérique ou symbolique.
 *
 * @param m			Pointeur sur la mesure
 * @param nb		Nombre de champ dans la table 'valeurs'
 * @param valeurs	Champs à traiter.
 */
static void store_fields( mesure_t* m, int nb, char** valeurs )
{
	if( nb != 2 ) {
		fprintf( stderr, "Erreur de syntaxe du parametre field %d", m->nb_elems );
		exit( EXIT_FAILURE );
	}
	m->elems = realloc( m->elems, (m->nb_elems+1) * sizeof(elem_t) );
	m->elems[m->nb_elems].name = strdup( valeurs[0] );

	m->elems[m->nb_elems].type = field;
	m->elems[m->nb_elems].oid.name = strdup( valeurs[1] );
	oid*	anOid = m->elems[m->nb_elems].oid.oid;
	size_t*	anOid_len = &m->elems[m->nb_elems].oid.oid_len;
	*anOid_len = MAX_OID_LEN;
	if( read_objid( valeurs[1], anOid, anOid_len ) != 1 ) {
		fprintf( stderr, "Conversion %s en OID impossible:\n", valeurs[1] );
		snmp_perror( "read_objid" );
		exit( EXIT_FAILURE );
	}
//	print_objid( anOid, *anOid_len );
//	print_oid( anOid, *anOid_len );
	m->nb_elems++;
}
/*!
 * \brief Stocker un OID à traiter par WALK.
 *
 *	Stocke le contenu d'un paramètre 'field':
 *		- field = nom, OID.
 *
 *  Le nom est un field (au sens InfluxDB) stocké dans la mesure correpondante.
 *  L'index (dernier champ) de l'oid est ajouté comme tag (au sens InfluxDB).
 *	L'OID peut être fourni sous une forme numérique ou symbolique.
 *	Un filtre peut être appliqué sur les index (dernier champ de l'OID) si
 * une liste est fournie.
 *
 * @param m			Pointeur sur la mesure
 * @param nb		Nombre de champ dans la table 'valeurs'
 * @param valeurs	Champs à traiter.
 */
static void store_tables( mesure_t* m, int nb, char** valeurs )
{
	if( nb != 2 ) {
		fprintf( stderr, "Erreur de syntaxe du parametre field %d", m->nb_elems );
		exit( EXIT_FAILURE );
	}
	m->elems = realloc( m->elems, (m->nb_elems+1) * sizeof(elem_t) );
	m->elems[m->nb_elems].name = strdup( valeurs[0] );

	m->elems[m->nb_elems].type = table;
	m->elems[m->nb_elems].oid.name = strdup(valeurs[1]);
	oid*	anOid = m->elems[m->nb_elems].oid.oid;
	size_t*	anOid_len = &m->elems[m->nb_elems].oid.oid_len;
	*anOid_len = MAX_OID_LEN;
	if( read_objid( valeurs[1], anOid, anOid_len ) != 1 ) {
		fprintf( stderr, "Conversion %s en OID impossible:\n", valeurs[1] );
		snmp_perror( "read_objid" );
		exit( EXIT_FAILURE );
	}
//	print_objid( anOid, *anOid_len );
//	print_oid( anOid, *anOid_len );
	m->nb_elems++;
}
#define MISSING_OPT_FMT "L'option \"%s\" de la section [%s] est obligatoire.\n"
/*!
 *	Analyse de la configuration.
 *
 *	Le fichier de configuration est divisé en section qui décrivent la connexion vers InfluxDB, la connexion SNMP et les mesures à collecter.
 *	Les sections sont introduites par un [nom de section]. La section comprend une suite d'options sous le forme vriable=valeur.
 *
 *	Sections valides:
 *		- influx: définition de la connexion à InfluxDB, URL, utilisateur, mot de passe, base.
 *		- snmp-auth: version, communauté ou utilisateur, mot de passe, niveau d'authentification, cryptage.
 *		- mesure: définit une mesure au sens influx, et le ou les champs, constitués des OIDs à récupérer.
 *
 *	Au moins une section 'mesure' est obligatoire, ainsi qu'une section 'influx' et une  section 'snmp-auth'.
 *
 *	Section influx:
 *		- url: URL de connexion à influxdb sous la forme 'http[s]://@IP_ou_DNS[:Port]'. Par défaut, le port = 8086.
 *		- base: nom de la base qui stocke les messures.
 *		- user:	utilisateur autorisé à accéder la base.
 *		- pass: mot de passe de l'utilisateur.
 *
 *	Les paramètres 'url' et 'base' sont obligatoires.
 *
 *	Section mesure:
 *		- nom: nom de la mesure dans la base influx (Lettres, chiffres, souslignés sont les éléments qui ne posent pas de problèmes).
 *		- field: nom du field (influx), OID
 *		- table: nom du field (influx), OID d'une table SNMP indexée.
 *
 *	Les paramètres 'nom' et 'au moins un 'field' et/ou une 'table' sont obligatoire.
 *
 *	Section snmp_auth:
 *		- version: La version du protocole SNMP a utiliser 1, 2c ou 3, définit le niveau de sécurité possible. [OBLIGATOIRE]
 *		- host:	@ IP ou DNS du serveur SNMP.  [OBLIGATOIRE]
 *		- communauté: Nom de la communauté en version 2c.  [OBLIGATOIREen version=2c]
 *		- niveau_sécurité: Le niveau de sécurité V3 exigé: "aucune", "auth_seule", "auth_cryptée" [OBLIGATOIRE en version=3]
 *		- user: Le nom d'utilisateur. [OBLIGATOIRE en version=3 quel que soit le niveau de sécurité.]
 *		- auth_proto: Le protocole de cryptage pour l'authentification "MD5" ou "SHA". [OBLIGATOIRE en version=3 et "auth_seule" ou "auth_cryptée"]
 *		- pass_auth: Le mot de passe pour l'authentification. [OBLIGATOIRE en version=3 et "auth_seule" ou "auth_cryptée"]
 *		- crypt_proto: le protocole de cryptage pour les échanges "DES" ou "AES". [OBLIGATOIRE en version=3 et "auth_cryptée"]
 *		- pass_crypt: Le mot de passe pour le cryptage des échanges. [OBLIGATOIRE en version=3 et "auth_cryptée"]
 *		- tags: Une liste de "tag=valeur" séparés par des virgules à ajouter à toutes les mesures.
 *
 * @param cfg		Le fichier de configuration.
 * @param config	La structure qui contient la configuration (doit être initialisée à zéro avant l'appel)..
 *
 * @return	0 en cas de succès. le programme stoppe en cas d'erreur.
 */
static int dispatche_configuration( struct _CONF_* cfg, _snmp2influxdb_config_t* config )
{
	config->frequency = 30;			/* Freq par défaut. */

	if( nb_sections_configuration( cfg, "influx" ) > 0 ) {
		char**	valeurs = NULL;
		if( valeurs_configuration( cfg, "influx", 0, "url", &valeurs ) > 0 ) {
			config->influx_host = strdup( valeurs[0] );
		} else {
			fprintf( stderr, "Section [influx]: Pas de paramètre obligatoire \"url\".\n" );
			exit( EXIT_FAILURE );
		}
		if( strstr( config->influx_host, "http" ) == NULL ) {
			fprintf( stderr, "Section [influx]: Format du paramètre \"url\": http[s]://@IP[:port]. Le port 8086 est par défaut.\n" );
			exit( EXIT_FAILURE );
		}
		if( valeurs_configuration( cfg, "influx", 0, "base", &valeurs ) > 0 ) {
			config->influx_base = strdup( valeurs[0] );
		} else {
			fprintf( stderr, "Section [influx]: Pas de paramètre obligatoire \"base\".\n" );
			exit( EXIT_FAILURE );
		}
		if( valeurs_configuration( cfg, "influx", 0, "user", &valeurs ) > 0 ) {
			config->influx_user = strdup( valeurs[0] );
		}
		if( valeurs_configuration( cfg, "influx", 0, "pass", &valeurs ) > 0 ) {
			config->influx_pass = strdup( valeurs[0] );
		}
		if( valeurs_configuration( cfg, "influx", 0, "freq", &valeurs ) > 0 ) {
			config->frequency = atoi( valeurs[0] );
			if( config->frequency <= 0 ) config->frequency = 30;
		}
	} else {
		fprintf( stderr, "Pas de section [influx]\n" );
		exit( EXIT_FAILURE );
	}

	if( nb_sections_configuration( cfg, "snmp-auth" ) <= 0 ) {
		fprintf( stderr, "Pas de section [snmp-auth]\n" );
		exit( EXIT_FAILURE );
	} else {
		char**	valeurs = NULL;
		config->version = 1;
		config->sec_level = 0;

		if( valeurs_configuration( cfg, "snmp-auth", 0, "version", &valeurs ) > 0 ) {
			config->version = !strcmp( valeurs[0], "3" ) ? 3 : !strcmp( valeurs[0], "2c" ) ? 2 : 1;
			if( config->version == 3 ) {
				if( valeurs_configuration( cfg, "snmp-auth", 0, "user", &valeurs ) > 0 ) {
					config->username = strdup( valeurs[0] );
				} else {
					fprintf( stderr, "Section [snmp-auth]: Le nom d'utilisateur est obligatoire.\n" );
					exit( EXIT_FAILURE );
				}
				if( valeurs_configuration( cfg, "snmp-auth", 0, "niveau_sécurité", &valeurs ) > 0 ) {
					if( !strcmp( valeurs[0] , "aucune" ) ) {
						config->sec_level = SNMP_SEC_LEVEL_NOAUTH;
					} else if( !strcmp( valeurs[0] , "auth_seule" ) ) {
						config->sec_level = SNMP_SEC_LEVEL_AUTHNOPRIV;
						if( valeurs_configuration( cfg, "snmp-auth", 0, "auth_proto", &valeurs ) > 0 ) {
							if( !strcmp( valeurs[0], "MD5" ) ) {
								config->AuthProto = strdup( "MD5" );
							} else if( !strcmp( valeurs[0], "SHA" ) ) {
								config->AuthProto = strdup( "SHA" );
							} else {
								fprintf( stderr, "Section [snmp-auth]: auth_proto = \"MD5\" ou \"SHA\".\n" );
								exit( EXIT_FAILURE );
							}
						}
						if( valeurs_configuration( cfg, "snmp-auth", 0, "pass_auth", &valeurs ) > 0 ) {
							config->AuthPass = strdup( valeurs[0] );
						} else {
							fprintf( stderr, "Section [snmp-auth]: L'option \"pass_auth\" est obligatoire pour le mode \"auth_seule\".\n" );
							exit( EXIT_FAILURE );
						}
					} else if( !strcmp( valeurs[0] , "auth_cryptée" ) ) {
						config->sec_level = SNMP_SEC_LEVEL_AUTHPRIV;
						if( valeurs_configuration( cfg, "snmp-auth", 0, "auth_proto", &valeurs ) > 0 ) {
							if( !strcmp( valeurs[0], "MD5" ) ) {
								config->AuthProto = strdup( "MD5" );
							} else if( !strcmp( valeurs[0], "SHA" ) ) {
								config->AuthProto = strdup( "SHA" );
							} else {
								fprintf( stderr, "Section [snmp-auth]: auth_proto = \"MD5\" ou \"SHA\".\n" );
								exit( EXIT_FAILURE );
							}
						}
						if( valeurs_configuration( cfg, "snmp-auth", 0, "pass_auth", &valeurs ) > 0 ) {
							config->AuthPass = strdup( valeurs[0] );
						} else {
							fprintf( stderr, "Section [snmp-auth]: L'option \"pass_auth\" est obligatoire pour le mode \"auth_cryptée\".\n" );
							exit( EXIT_FAILURE );
						}
						if( valeurs_configuration( cfg, "snmp-auth", 0, "crypt_proto", &valeurs ) > 0 ) {
							if( !strcmp( valeurs[0], "AES" ) ) {
								config->CryptProto = strdup( "AES" );
							} else if( !strcmp( valeurs[0], "DES" ) ) {
								config->CryptProto = strdup( "DES" );
							} else {
								fprintf( stderr, "Section [snmp-auth]: crypt_proto = \"DES\" ou \"AES\".\n" );
								exit( EXIT_FAILURE );
							}
						}
						if( valeurs_configuration( cfg, "snmp-auth", 0, "pass_crypt", &valeurs ) > 0 ) {
							config->CryptPass = strdup( valeurs[0] );
						} else {
							fprintf( stderr, "Section [snmp-auth]: L'option \"pass_crypt\" est obligatoire pour le mode \"auth_cryptée\".\n" );
							exit( EXIT_FAILURE );
						}
					}
				}
			}
			if( valeurs_configuration( cfg, "snmp-auth", 0, "communauté", &valeurs ) > 0 ) {
				config->community = strdup( valeurs[0] );
			}
			if( valeurs_configuration( cfg, "snmp-auth", 0, "host", &valeurs ) > 0 ) {
				config->peername = strdup( valeurs[0] );
			} else {
				fprintf( stderr, "Section [snmp-auth]: Pas de paramètre obligatoire \"host\".\n" );
				exit( EXIT_FAILURE );
			}
			int ret;
			if( (ret = valeurs_configuration( cfg, "snmp-auth", 0, "tags", &valeurs )) > 0 ) {
				for( int i=0; i < ret; i++ ) {
					int index = config->tags.nb;
					config->tags.data = realloc( config->tags.data, (config->tags.nb + 1)*sizeof(*config->tags.data) );
					if( config->tags.data == NULL ) {
						fprintf( stderr, "Section [snmp-auth]: allocation de la liste des tags impossible.\n" );
						exit( EXIT_FAILURE );
					} else {
						config->tags.nb++;
					}
					char* pt = strchr( valeurs[i], '=' );
					if( pt ) {
						*pt='\0';
						config->tags.data[index].var = strdup( valeurs[i] );
						config->tags.data[index].val = strdup( pt+1 );
					}
				}
			}
		} else {
			fprintf( stderr, "Section [snmp-auth]: Pas de paramètre obligatoire \"version\" = 1 ou 2c ou 3.\n" );
			exit( EXIT_FAILURE );
		}
	}

	int nb = nb_sections_configuration( cfg, "mesure" );
	if( nb != 0 ) {
		config->oids = new_mesure( nb );
		config->nb_oids = nb;

		char**	valeurs = NULL;
		for( int i=0; i < nb; i++ ) {
			if( valeurs_configuration_n( cfg, "mesure", i, "field", 0, NULL ) <= 0 &&
					valeurs_configuration_n( cfg, "mesure", i, "table", 0, NULL ) <= 0 ) {
				fprintf( stderr, "La section [mesure] n° %d n'a ni option \"field\", ni option \"table\".\n", i );
				exit( EXIT_FAILURE );
			}

			if( valeurs_configuration( cfg, "mesure", i, "nom", &valeurs ) > 0 ) {
				config->oids[i].name = strdup(valeurs[0]);
			} else {
				fprintf( stderr, "Mesure n° %d: pas de champ \"nom\" pour cette mesure!\n", i );
				exit( EXIT_FAILURE );
			}
			/* Lire les paramètres 'field', 'table' et/ou 'index'. */
			int nb_index = valeurs_configuration( cfg, "mesure", i, "index", &valeurs );

			if( nb_index > 0  ) {
				store_index( config->oids+i, nb_index, valeurs );
			}

			int no_field = 0;
			while( 1 ) {
				int nb_fields = valeurs_configuration_n( cfg, "mesure", i, "field", no_field, &valeurs );
				if( nb_fields > 0 ) {
					store_fields( config->oids+i, nb_fields, valeurs );
				} else {
					break;
				}
				no_field++;
			}

			int no_table = 0;
			while( 1 ) {
				int nb_tables = valeurs_configuration_n( cfg, "mesure", i, "table", no_table, &valeurs );
				if( nb_tables > 0 ) {
					store_tables( config->oids+i, nb_tables, valeurs );
				} else {
					break;
				}
				no_table++;
			}
		}
	}

	return 0;
}
/*
 * simple printing of returned data
 */
/*!
 * \brief	Afficher le résultat d'une requête SNMP.
 *
 * @param status	Status de la requête SNMP.
 * @param sp		Session Net-SNMP.
 * @param pdu		Contenu de la réponse.
 *
 * @return	1 si le status STAT_SUCCESS, 0 sinon.
 */
static int print_result( int status, struct snmp_session *sp, struct snmp_pdu *pdu )
{
	char					buf[1024];
	struct variable_list*	vp;
	struct tm*				tm;
	struct timeval			now;
	struct timezone			tz;
	int						ret = 0;

	fprintf( stdout, "result = (\n" );
	gettimeofday( &now, &tz );
	tm = localtime( &now.tv_sec );
	switch( status ) {
	case STAT_SUCCESS:
		vp = pdu->variables;
		if (pdu->errstat == SNMP_ERR_NOERROR) {
			while (vp) {
				snprint_variable(buf, sizeof(buf), vp->name, vp->name_length, vp);
				fprintf(stdout, "  %.2d:%.2d:%.2d.%.6lu %s: %s\n",
						tm->tm_hour, tm->tm_min, tm->tm_sec, now.tv_usec, sp->peername, buf);
				vp = vp->next_variable;
			}
		} else {
			for( int ix = 1; vp && ix != pdu->errindex; vp = vp->next_variable, ix++ );
			fprintf(stdout, "  %.2d:%.2d:%.2d.%.6lu ", tm->tm_hour, tm->tm_min, tm->tm_sec, now.tv_usec );
			if( vp )
				snprint_objid( buf, sizeof(buf), vp->name, vp->name_length );
			else
				strcpy( buf, "(none)" );
			fprintf( stdout, "%s: %s: %s\n", sp->peername, buf, snmp_errstring(pdu->errstat) );
		}
		ret = 1;
		break;
	case STAT_TIMEOUT:
		fprintf(stdout, "  %.2d:%.2d:%.2d.%.6lu ", tm->tm_hour, tm->tm_min, tm->tm_sec, now.tv_usec );
		fprintf( stdout, "%s: Timeout\n", sp->peername );
		ret = 0;
		break;
	case STAT_ERROR:
		fprintf(stdout, "  %.2d:%.2d:%.2d.%.6lu ", tm->tm_hour, tm->tm_min, tm->tm_sec, now.tv_usec );
		snmp_perror( sp->peername );
		ret = 0;
		break;
	}
	fprintf( stdout, ")\n" );
	return ret;
}
#if 0
/*!
 * \brief Décodification et affichage d'un contenu JSON
 *
 * @param series	L'objet JSON a afficher.
 */
static void display_series( json_t* series )
{
	size_t	index;
	json_t*	value;

	json_array_foreach( series, index, value ) {
		json_t* name = json_object_get( value, "name" );
		if( name && Verbose )
			fprintf( stdout, "%16.16s\n-----------------\n", json_string_value( name ) );
		json_t* columns =  json_object_get( value, "columns" );
		size_t	colnum;
		json_t*	colval;
		json_array_foreach( columns, colnum, colval ) {
			if( Verbose ) fprintf( stdout, "%*s", colnum==0?20:16, json_string_value(colval) );
		}
		if( Verbose ) fprintf( stdout, "\n\n" );
		json_t* values =  json_object_get( value, "values" );
		size_t	valnum;
		json_t*	valval;
		json_array_foreach( values, valnum, valval ) {
			json_array_foreach( valval, colnum, colval ) {
				if( colval != NULL ) {
					switch( json_typeof(colval ) ) {
					case JSON_STRING:
						fprintf( stdout, "%*.20s", colnum==0?20:16, json_string_value(colval) );
						break;
					case JSON_INTEGER:
						fprintf( stdout, "%16lld", json_integer_value(colval) );
						break;
					case JSON_REAL:
						fprintf( stdout, "%16.2f", json_real_value(colval) );
						break;
					case JSON_TRUE:
						fprintf( stdout, "%16.16s", "True" );
						break;
					case JSON_FALSE:
						fprintf( stdout, "%16.16s", "False" );
						break;
					case JSON_NULL:
						fprintf( stdout, "%16.16s", "((Null))" );
						break;
					case JSON_ARRAY:
					case JSON_OBJECT:
						fprintf( stdout, "%s\n", json_dumps(colval, JSON_INDENT(2)) );
					}
				}
			}
			if( Verbose ) fprintf( stdout, "\n" );
		}
		if( Verbose ) fprintf( stdout, "\n" );
	}
}
#endif
/*!
 * \brief Afficher la réponse au write influxdb.
 *
 *	Décode et affiche le contenu du JSON retourné lors du InfluxDB_write().
 *
 * @param resp	Le contenu de la réponse.
 */
static void display_response( char *resp )
{
	if( !resp ) return;

	json_error_t	jerror;
	json_t*			retour = json_loads( resp, JSON_DECODE_ANY|JSON_DISABLE_EOF_CHECK, &jerror );

	if( !retour ) {
		if( Verbose )
			fprintf( stderr, "Response:%s\nErreur %d-%d: %s\n", resp, jerror.line, jerror.column, jerror.text );
		else
			syslog( LOG_ERR, "Erreur ligne-col %d-%d: %s\n", jerror.line, jerror.column, jerror.text );
	} else {
		if( Verbose ) fprintf( stderr, "\n%s\n", json_dumps( retour, JSON_COMPACT ) );
#if 0
		json_t* result = json_object_get( retour, "error" );
		if( result ) {
			size_t	index;
			json_t*	value;

			json_array_foreach( result, index, value ) {
				json_t* error = json_object_get( value, "error" );
				if( error ) {
					if( Verbose )
						fprintf( stderr, "ERREUR: %s\n", json_string_value( error ) );
					else
						syslog( LOG_ERR, "ERREUR: %s", json_string_value( error ) );
				} else {
					json_t* series = json_object_get( value, "series" );
					if( series ) display_series( series );
				}
			}
		}
#endif
	}
	json_decref( retour );
}

static void display_configuration( _snmp2influxdb_config_t* cfg )
{
	fprintf( stderr, "[influx]\n  url=%s\n  base=%s\n  freq=%lu\n", cfg->influx_host, cfg->influx_base, cfg->frequency );
	if( cfg->influx_user ) {
		fprintf( stderr, "  user=%s\npass=%s\n", cfg->influx_user, cfg->influx_pass );
	}

	fprintf( stderr, "\n[snmp-auth]\n  version=%s\n  host=%s\n",
			cfg->version == 1 ? "1" : cfg->version == 2 ? "2c" : cfg->version == 3 ? "3" : "????", cfg->peername );
	if( cfg->version == 2 ) {
		fprintf( stderr, "  communauté=%s\n", cfg->community );
	} else if( cfg->version == 3 ) {
		fprintf( stderr, "  user = %s\n", cfg->username );
		if( cfg->sec_level == SNMP_SEC_LEVEL_NOAUTH ) {
			fprintf( stderr, "  niveau_sécurité=aucune\n" );
		} else if( cfg->sec_level != SNMP_SEC_LEVEL_NOAUTH ) {
			fprintf( stderr, "  niveau_sécurité=%s\n  auth_proto = %s\n  pass_auth = %s\n",
					cfg->sec_level == SNMP_SEC_LEVEL_AUTHPRIV ? "auth_cryptée" : "auth_seule", cfg->AuthProto, cfg->AuthPass );
			if( cfg->sec_level == SNMP_SEC_LEVEL_AUTHPRIV ) {
				fprintf( stderr, "  crypt_proto = %s\n  pass_crypt = %s\n", cfg->CryptProto, cfg->CryptPass );
			}
		}
	}
	if( cfg->tags.nb ) {
		fprintf( stderr, "  tags" );
		char c = '=';
		for( int i = 0; i < cfg->tags.nb; i++ ) {
			fprintf( stderr, "%c\"%s=%s\"", c, cfg->tags.data[i].var, cfg->tags.data[i].val );
			c = ',';
		}
		fprintf( stderr, "\n" );
	}

	if( cfg->nb_oids ) {
		for( int i = 0; i < cfg->nb_oids; i++ ) {
			mesure_t* m = cfg->oids+i;
			fprintf( stderr, "\n[mesure]\n  nom = %s\n", m->name );
			if( m->nb_index_filter ) {
				fprintf( stderr, "  index=" );
				for( int j=0; j < m->nb_index_filter; j++ ) {
					fprintf( stderr, "%lu", m->index_filter[j] );
				}
			}
			for( int j=0; j < m->nb_elems; j++ ) {
				elem_t* e = m->elems+j;
				fprintf( stderr, e->type == field ? "  field" : "  table=" );
				fprintf( stderr, "=%s,%s (", e->name, e->oid.name );
				for( int k = 0; k < e->oid.oid_len-1; k++ ) fprintf( stderr, "%lu.", e->oid.oid[k] );
				fprintf( stderr, "%lu)\n", e->oid.oid[e->oid.oid_len-1] );
			}
		}
	}
	fprintf( stderr, "\n" );
}
/*!
 * \brief	Handler for TERM and QUIT signals.
 *
 * @param signal
 */
static void do_term( int signal )
{
	if( Verbose ) fprintf( stderr, "%s: terminaison demandée sur %d\n", __func__, signal );
	TerminaisonAsked = 1;
}

#ifndef NETSNMP_USMAUTH_BASE_OID
#  define NETSNMP_USMAUTH_BASE_OID 1,3,6,1,6,3,10,1,1
#endif
#ifndef NETSNMP_USMAUTH_HMACMD5
#  define NETSNMP_USMAUTH_HMACMD5	1
#endif
#ifndef NETSNMP_USMAUTH_HMACSHA1
#  define NETSNMP_USMAUTH_HMACSHA1	3
#endif

int main( int argc, char* argv[] )
{
	static struct option long_options[] = {
			{ "conf",		required_argument,	0,	'c' },
			{ "version",	no_argument,		0,	'V' },
			{ "verbose",	no_argument,		0,	'v' },
			{ "no_daemon",	no_argument,		0,	'n' },
			{ 0,			0,					0,	 0  }
	};
	char*	conf_file = "snmp2influx.conf";
	int		daemonize = 1;
	int c;

	while( 1 ) {
		int option_index = 0;
		c = getopt_long( argc, argv, "c:vnV", long_options, &option_index );

		if( c == -1 ) break;

		switch( c ) {
		case 'c':
			conf_file = optarg;
			break;
		case 'v':
			Verbose = 1;
			daemonize = 0;
			break;
		case 'n':
			daemonize = 0;
			break;
		case 'V':
			fprintf( stderr, "version: %s build: %s\n", VERSION, BUILD_TIME );
			return 0;
		default:
			fprintf( stderr, "Option inconnue: %c\n", c );
			exit( EXIT_FAILURE );
		}
	}

	/* Initialize all, and the SNMP library */
	memset( &Config, '\0', sizeof(Config) );
	struct sigaction terminaison;
	sigemptyset( &terminaison.sa_mask );
	terminaison.sa_flags = SA_RESTART;
	terminaison.sa_handler = do_term;
	sigaction( SIGTERM, &terminaison, NULL );
	sigaction( SIGQUIT, &terminaison, NULL );
	init_snmp( "Snmp2InfluxDB" );
	init_mib();
	(void)read_all_mibs();
	// Add hostname as tag collecteur.
	gethostname( HostName, HOST_NAME_MAX );
	Config.tags.data = realloc( Config.tags.data, (Config.tags.nb + 1)*sizeof(*Config.tags.data) );
	if( Config.tags.data == NULL ) {
		fprintf( stderr, "Cannot add tag \"collecteur\" to global list.\n" );
		exit( EXIT_FAILURE );
	} else {
		Config.tags.nb++;
	}
	Config.tags.data[0].var = strdup("collecteur");
	Config.tags.data[0].val = strdup(HostName);

	struct _CONF_*	cfg = parse_configuration( conf_file, NULL );

	if( cfg == NULL ) {
		exit( EXIT_FAILURE );
	}
	/* Parse configuration. */
	dispatche_configuration( cfg, &Config );
	detruire_configuration( cfg );
	if( Verbose ) display_configuration( &Config );

	SOCK_STARTUP;			/* Windows hack */

	/* Daemonize */
	if( daemonize ) {
		pid_t pid = fork();
		if( pid < 0 ) {
			perror( "daemonize" );
			exit( EXIT_FAILURE );
		}
		if( pid == 0 ) {
			for( int i=0; i<5; i++ ) close(i);
			setsid();
		} else {
			fprintf( stdout, "Starting PID: %d\n", pid );
			exit( EXIT_SUCCESS );
		}
	}
	/* Initialize InfluxDB session. */
	char* pt = NULL;
	char* url = NULL;
	if( (pt = strrchr( Config.influx_host, ':' )) != NULL ) {
		url = strdup(Config.influx_host);
	} else {
		url = malloc( strlen(Config.influx_host)+8 );
		sprintf( url, "%s:8086", Config.influx_host );
	}
	if( Verbose ) {
		fprintf( stderr, "Store measurements in \"%s\" on %s.\n", Config.influx_base, url );
	}
	InfluxDBClient*	clt = InfluxDBClientNew();

	InfluxDB_setConnectionParamsV1( clt, url, Config.influx_user, Config.influx_pass, Config.influx_base );
	for( int i = 0; i < Config.tags.nb; i++ ) {
		InfluxDB_addGlobalTag( clt, Config.tags.data[i].var, Config.tags.data[i].val );
		if( Verbose ) fprintf( stderr, "add global tag %d: %s = %s\n", i, Config.tags.data[i].var, Config.tags.data[i].val );
	}

	/* Initialize a "session" that defines who we're going to talk to */
	struct snmp_session	session;

	snmp_sess_init( &session );
	session.peername = Config.peername;

	if( Config.version == 3 ) {
		session.version=SNMP_VERSION_3;

		/* set the SNMPv3 user name */
		session.securityName = strdup( Config.username );
		session.securityNameLen = strlen( session.securityName );

		/* set the security level */
		session.securityLevel = Config.sec_level;

		if( session.securityLevel == SNMP_SEC_LEVEL_AUTHNOPRIV || session.securityLevel == SNMP_SEC_LEVEL_AUTHPRIV ) {
			/* set the authentication method */
			oid usmHMACMD5AuthProtocol[10] = { NETSNMP_USMAUTH_BASE_OID, NETSNMP_USMAUTH_HMACMD5 };
			oid usmHMACSHA1AuthProtocol[10] = { NETSNMP_USMAUTH_BASE_OID, NETSNMP_USMAUTH_HMACSHA1 };
			if( !strcmp( Config.AuthProto, "MD5" ) ) {
				session.securityAuthProto = usmHMACMD5AuthProtocol;
				session.securityAuthProtoLen = USM_AUTH_PROTO_MD5_LEN;
			} else {
				session.securityAuthProto = usmHMACSHA1AuthProtocol;
				session.securityAuthProtoLen = USM_AUTH_PROTO_SHA_LEN;
			}
			session.securityAuthKeyLen = USM_AUTH_KU_LEN;
			if( generate_Ku( session.securityAuthProto,
					session.securityAuthProtoLen,
					(u_char *)Config.AuthPass, strlen(Config.AuthPass),
					session.securityAuthKey,
					&session.securityAuthKeyLen ) != SNMPERR_SUCCESS) {
				netsnmp_sess_log_error( LOG_ERR, argv[0], &session );

				exit( EXIT_FAILURE );
			}
			if( session.securityLevel == SNMP_SEC_LEVEL_AUTHPRIV ) {
				/* set the privacy protocol */
				oid usmDESPrivProtocol[10] = { 1, 3, 6, 1, 6, 3, 10, 1, 2, 2 };
				oid usmAESPrivProtocol[10] = { 1, 3, 6, 1, 6, 3, 10, 1, 2, 4 };
				if( !strcmp( Config.CryptProto, "DES" ) ) {
					session.securityPrivProto = usmDESPrivProtocol;
					session.securityPrivProtoLen = USM_PRIV_PROTO_DES_LEN;
				} else {
					session.securityPrivProto = usmAESPrivProtocol;
					session.securityPrivProtoLen = USM_PRIV_PROTO_AES_LEN;
				}
				session.securityPrivKeyLen = USM_PRIV_KU_LEN;
				if( generate_Ku( session.securityAuthProto,
						session.securityAuthProtoLen,
						(u_char *)Config.CryptPass, strlen(Config.CryptPass),
						session.securityPrivKey,
						&session.securityPrivKeyLen ) != SNMPERR_SUCCESS) {
					netsnmp_sess_log_error( LOG_ERR, argv[0], &session );

					exit( EXIT_FAILURE );
				}
			}
		}
	} else if( Config.version == 2 ) {
		session.version=SNMP_VERSION_2c;

		/* set the SNMPv2c community name used for authentication */
		session.community = (u_char*)Config.community;
		session.community_len = strlen( (char*)session.community );
	} else {
		session.version=SNMP_VERSION_1;

		/* set the SNMPv1 community name used for authentication */
		session.community = (u_char*)Config.community;
		session.community_len = strlen( (char*)session.community );
	}

	struct snmp_session* ss = snmp_open( &session );

	if( !ss ) {
		netsnmp_sess_log_error( LOG_ERR, "snmp session opening", &session );

		SOCK_CLEANUP;
		exit( EXIT_FAILURE );
	}

	while( !TerminaisonAsked ) {
		/* Parcours des mesures. */
		for( int i = 0; i < Config.nb_oids; i++ ) {
			netsnmp_pdu *pdu = snmp_pdu_create( SNMP_MSG_GET );

			mesure_t*	m = Config.oids+i;
			if( m->nb_index_filter == 0 ) {
				/* Ajouter les requêtes 'field' */
				for( int j=0; j < m->nb_elems; j++ ) {
					snmp_add_null_var( pdu, m->elems[j].oid.oid, m->elems[j].oid.oid_len );
				}
			} else {
				/* Ajouter les requêtes 'table' dans new_oids[index][table] */
				oid new_oid[MAX_OID_LEN];
				for( int ind=0; ind < m->nb_index_filter; ind++ ) {
					for( int j=0; j < m->nb_elems;j++ ) {
						size_t	oid_len = m->elems[j].oid.oid_len;
						memmove( new_oid, m->elems[j].oid.oid, oid_len*sizeof(oid) );
						new_oid[oid_len] = m->index_filter[ind];
						snmp_add_null_var( pdu, new_oid, ++oid_len );
					}
				}
			}

			/* Exécuter la requête. */
			netsnmp_pdu*	response = NULL;

			int status = snmp_synch_response( ss, pdu, &response );

			if( Verbose ) print_result( status, &session, response );

			if( status == STAT_SUCCESS && pdu->errstat == SNMP_ERR_NOERROR ) {
				struct variable_list*	vp = response->variables;
				int						count = 0;
				if (pdu->errstat == SNMP_ERR_NOERROR) {
					while( vp ) {
						char*	f_name = NULL;

						if( m->nb_index_filter == 0 ) {
							f_name = m->elems[count].name;
						} else {
							f_name = m->elems[count%m->nb_elems].name;
						}

						switch( vp->type ) {
						case ASN_COUNTER64:
							/* On devrait avoir InfluxDB_addUIntField mais influxdb v1 n'aime pas. */
							InfluxDB_addIntField( clt, f_name, (((uint64_t)(vp->val.counter64->high)<<32) + vp->val.counter64->low) );
							break;
						case ASN_GAUGE:
						case ASN_UINTEGER:
						case ASN_COUNTER:
							/* On devrait avoir InfluxDB_addUIntField mais influxdb v1 n'aime pas. */
							InfluxDB_addIntField( clt, f_name, (int64_t)*vp->val.integer );
							break;
						case ASN_TIMETICKS:
						case ASN_INTEGER:
							InfluxDB_addIntField( clt, f_name, (int64_t)*vp->val.integer );
							break;
						case ASN_OCTET_STR:
							if( vp->val.string && *vp->val.string != '\0' )
								InfluxDB_addField( clt, f_name, (char*)vp->val.string );
							break;
						case ASN_BIT_STR:
							if( vp->val.bitstring && *vp->val.bitstring != '\0' )
								InfluxDB_addField( clt, f_name, (char*)vp->val.bitstring );
							break;
						case ASN_OPAQUE_FLOAT:
							InfluxDB_addDoubleField( clt, f_name, (double)*vp->val.floatVal );
							break;
						case ASN_OPAQUE_DOUBLE:
							InfluxDB_addDoubleField( clt, f_name, (double)*vp->val.doubleVal );
							break;
						default:
							fprintf( stderr, "type non géré: %02x\n", vp->type );
							break;
						}
						char buf[32];
						snprintf( buf, sizeof(buf), "%d", (int)vp->name[vp->name_length-1] );
						InfluxDB_addTag( clt, "index", buf );
						InfluxDB_addMeasure( clt, m->name );

						vp = vp->next_variable;
						count++;
					}
				}
			}

			if( response ) snmp_free_pdu( response );
		}

		if( Verbose ) fprintf( stderr, "%s", InfluxDB_to_string(clt) );

		size_t	len;
		if( InfluxDB_write( clt ) ) {
			display_response( (char*)InfluxDB_get_response( clt, &len ) );
		}
		if( !TerminaisonAsked ) sleep( Config.frequency );
	}

	InfluxDB_write( clt );

	if( url ) free( url );
	if( Config.AuthPass ) free( Config.AuthPass );
	if( Config.AuthProto ) free( Config.AuthProto );
	if( Config.CryptPass ) free( Config.CryptPass );
	if( Config.CryptProto ) free( Config.CryptProto );
	if( Config.community ) free( Config.community );
	if( Config.influx_base ) free( Config.influx_base );
	if( Config.influx_host ) free( Config.influx_host );
	if( Config.influx_pass ) free( Config.influx_pass );
	if( Config.influx_user ) free( Config.influx_user );
	if( Config.peername ) free( Config.peername );
	if( Config.username ) free( Config.username );
	if( Config.tags.nb ) {
		for( int i = 0; i < Config.tags.nb; i++ ) {
			if( Config.tags.data[i].var ) free( Config.tags.data[i].var );
			if( Config.tags.data[i].val ) free( Config.tags.data[i].val );
		}
		free( Config.tags.data );
	}

	snmp_close( ss );
	InfluxDBClientDestroy( clt );

	SOCK_CLEANUP;

	return EXIT_SUCCESS;
}
