/*!
 * \file parse_config.h
 * 
 * 						__      ______  ______
 * 					   /  \    / _____|| _____|
 * 					  / /\ \  | /      | |___
 * 					 / /  \ \ | |      |  ___|
 * 					/ /____\ \| |      | | 
 * 				   / /______\ \ \_____ | |____
 * 				  /_/        \_\______||______|
 *
 * \brief Entete de l'analyseur de fichier de configuration
 *
 * @(#)  $Id$  ECOC
 *
 * \author ECOCHARD Jean-Paul <jp.ecoc@free.fr>
 * \date   jeu fév 27 20:42:17 CET 2003
 */
#ifndef _PARSE_CONFIG_H
# define _PARSE_CONFIG_H

#ifdef _WIN32
  /* You should define DLL_BUILD *only* when building the DLL. */
  #ifdef DLL_BUILD
    #define DLLAPI __declspec(dllexport)
  #else
    #define DLLAPI __declspec(dllimport)
  #endif

  /* Define calling convention in one place, for convenience. */
  #define DLLCALL __cdecl

#else /* _WIN32 not defined. */

  /* Define with no value on non-Windows OSes. */
  #define DLLAPI
  #define DLLCALL

#endif

#ifdef __cplusplus
extern "C" {
#endif

/*!
 *	Structure du fichier de configuration.
 */
/*!
 *	Type des fonctions d'erreur.
 */
typedef void (* ERR_FONCT)(int lig, char *msg);
/*! \brief Analyse et découpe le fichier de configuration. */
DLLAPI struct _CONF_ DLLCALL *parse_configuration( char *conf_file, ERR_FONCT log_err );
/*! \brief Accèder aux noms de toutes les sections. */
DLLAPI int	DLLCALL liste_sections_configuration( struct _CONF_ *conf, char ***noms );
/*! \brief Donne de nombre de sections identiques du fichier de configuration.*/
DLLAPI int	DLLCALL nb_sections_configuration( struct _CONF_ *, char *section );
/*! \brief Donne le nombre de paramètres d'une section */
DLLAPI int	DLLCALL nb_parametres_configuration( struct _CONF_ *, char *section, int num );
/*! \brief Accèder aux valeurs du nième paramètre. */
DLLAPI int	DLLCALL valeurs_configuration_n( struct _CONF_ *conf, char *section, int num, char *parametre, int num_par, char ***valeurs );
/*! \brief Accèder aux valeurs d'un paramètre. */
DLLAPI int	DLLCALL valeurs_configuration( struct _CONF_ *conf, char *section, int num, char *parametre, char ***valeurs );
/*! \brief Accèder aux noms de tous les paramètres d'une section. */
DLLAPI int	DLLCALL liste_parametres_configuration( struct _CONF_ *conf, char *section, int num, char ***noms );
DLLAPI void	DLLCALL detruire_liste_configuration( char ***noms );
/*! \brief Détruire une configuration. */
DLLAPI void	DLLCALL detruire_configuration( struct _CONF_ * );

#ifdef __cplusplus
}
#endif

#endif
