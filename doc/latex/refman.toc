\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\babel@toc {french}{}\relax 
\contentsline {chapter}{\numberline {1}Index des structures de données}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Structures de données}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}Index des fichiers}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Liste des fichiers}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}Documentation des structures de données}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}Référence de la structure \discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}CONF\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}}{5}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Description détaillée}{5}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Documentation des champs}{6}{subsection.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.2.1}nb\_sections}{6}{subsubsection.3.1.2.1}%
\contentsline {subsubsection}{\numberline {3.1.2.2}sections}{6}{subsubsection.3.1.2.2}%
\contentsline {subsubsection}{\numberline {3.1.2.3}type}{6}{subsubsection.3.1.2.3}%
\contentsline {section}{\numberline {3.2}Référence de la structure \discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}elem\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}}{6}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Documentation des énumérations membres}{7}{subsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.1.1}anonymous enum}{7}{subsubsection.3.2.1.1}%
\contentsline {subsection}{\numberline {3.2.2}Documentation des champs}{7}{subsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.2.1}name}{7}{subsubsection.3.2.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2.2}oid}{7}{subsubsection.3.2.2.2}%
\contentsline {subsubsection}{\numberline {3.2.2.3}}{7}{subsubsection.3.2.2.3}%
\contentsline {section}{\numberline {3.3}Référence de la structure \discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}SECTION\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}}{8}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Description détaillée}{8}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Documentation des champs}{8}{subsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.2.1}nb\_params}{8}{subsubsection.3.3.2.1}%
\contentsline {subsubsection}{\numberline {3.3.2.2}nom}{8}{subsubsection.3.3.2.2}%
\contentsline {subsubsection}{\numberline {3.3.2.3}params}{9}{subsubsection.3.3.2.3}%
\contentsline {section}{\numberline {3.4}Référence de la structure \discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}snmp2influxdb\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}config\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}}{9}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Documentation des champs}{10}{subsection.3.4.1}%
\contentsline {subsubsection}{\numberline {3.4.1.1}AuthPass}{10}{subsubsection.3.4.1.1}%
\contentsline {subsubsection}{\numberline {3.4.1.2}AuthProto}{10}{subsubsection.3.4.1.2}%
\contentsline {subsubsection}{\numberline {3.4.1.3}community}{10}{subsubsection.3.4.1.3}%
\contentsline {subsubsection}{\numberline {3.4.1.4}CryptPass}{10}{subsubsection.3.4.1.4}%
\contentsline {subsubsection}{\numberline {3.4.1.5}CryptProto}{10}{subsubsection.3.4.1.5}%
\contentsline {subsubsection}{\numberline {3.4.1.6}frequency}{10}{subsubsection.3.4.1.6}%
\contentsline {subsubsection}{\numberline {3.4.1.7}influx\_base}{10}{subsubsection.3.4.1.7}%
\contentsline {subsubsection}{\numberline {3.4.1.8}influx\_host}{11}{subsubsection.3.4.1.8}%
\contentsline {subsubsection}{\numberline {3.4.1.9}influx\_pass}{11}{subsubsection.3.4.1.9}%
\contentsline {subsubsection}{\numberline {3.4.1.10}influx\_user}{11}{subsubsection.3.4.1.10}%
\contentsline {subsubsection}{\numberline {3.4.1.11}nb\_oids}{11}{subsubsection.3.4.1.11}%
\contentsline {subsubsection}{\numberline {3.4.1.12}oids}{11}{subsubsection.3.4.1.12}%
\contentsline {subsubsection}{\numberline {3.4.1.13}peername}{11}{subsubsection.3.4.1.13}%
\contentsline {subsubsection}{\numberline {3.4.1.14}sec\_level}{11}{subsubsection.3.4.1.14}%
\contentsline {subsubsection}{\numberline {3.4.1.15}tags}{11}{subsubsection.3.4.1.15}%
\contentsline {subsubsection}{\numberline {3.4.1.16}username}{12}{subsubsection.3.4.1.16}%
\contentsline {subsubsection}{\numberline {3.4.1.17}version}{12}{subsubsection.3.4.1.17}%
\contentsline {section}{\numberline {3.5}Référence de la structure \discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}snmp2influxdb\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}oid\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}}{12}{section.3.5}%
\contentsline {subsection}{\numberline {3.5.1}Documentation des champs}{12}{subsection.3.5.1}%
\contentsline {subsubsection}{\numberline {3.5.1.1}name}{12}{subsubsection.3.5.1.1}%
\contentsline {subsubsection}{\numberline {3.5.1.2}oid}{12}{subsubsection.3.5.1.2}%
\contentsline {subsubsection}{\numberline {3.5.1.3}oid\_len}{12}{subsubsection.3.5.1.3}%
\contentsline {section}{\numberline {3.6}Référence de la structure tags\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}::\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}tag\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}}{13}{section.3.6}%
\contentsline {subsection}{\numberline {3.6.1}Documentation des champs}{13}{subsection.3.6.1}%
\contentsline {subsubsection}{\numberline {3.6.1.1}val}{13}{subsubsection.3.6.1.1}%
\contentsline {subsubsection}{\numberline {3.6.1.2}var}{13}{subsubsection.3.6.1.2}%
\contentsline {section}{\numberline {3.7}Référence de la structure mesure}{13}{section.3.7}%
\contentsline {subsection}{\numberline {3.7.1}Documentation des champs}{14}{subsection.3.7.1}%
\contentsline {subsubsection}{\numberline {3.7.1.1}elems}{14}{subsubsection.3.7.1.1}%
\contentsline {subsubsection}{\numberline {3.7.1.2}index\_filter}{14}{subsubsection.3.7.1.2}%
\contentsline {subsubsection}{\numberline {3.7.1.3}name}{14}{subsubsection.3.7.1.3}%
\contentsline {subsubsection}{\numberline {3.7.1.4}nb\_elems}{14}{subsubsection.3.7.1.4}%
\contentsline {subsubsection}{\numberline {3.7.1.5}nb\_index\_filter}{14}{subsubsection.3.7.1.5}%
\contentsline {section}{\numberline {3.8}Référence de la structure PARAM}{14}{section.3.8}%
\contentsline {subsection}{\numberline {3.8.1}Description détaillée}{15}{subsection.3.8.1}%
\contentsline {subsection}{\numberline {3.8.2}Documentation des champs}{15}{subsection.3.8.2}%
\contentsline {subsubsection}{\numberline {3.8.2.1}nb\_valeurs}{15}{subsubsection.3.8.2.1}%
\contentsline {subsubsection}{\numberline {3.8.2.2}nom}{15}{subsubsection.3.8.2.2}%
\contentsline {subsubsection}{\numberline {3.8.2.3}valeurs}{15}{subsubsection.3.8.2.3}%
\contentsline {section}{\numberline {3.9}Référence de la structure tags}{15}{section.3.9}%
\contentsline {subsection}{\numberline {3.9.1}Documentation des champs}{16}{subsection.3.9.1}%
\contentsline {subsubsection}{\numberline {3.9.1.1}data}{16}{subsubsection.3.9.1.1}%
\contentsline {subsubsection}{\numberline {3.9.1.2}nb}{16}{subsubsection.3.9.1.2}%
\contentsline {chapter}{\numberline {4}Documentation des fichiers}{17}{chapter.4}%
\contentsline {section}{\numberline {4.1}Référence du fichier src/parse\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}config.c}{17}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Description détaillée}{18}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Documentation des macros}{19}{subsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.2.1}MY\_CLOSE}{19}{subsubsection.4.1.2.1}%
\contentsline {subsubsection}{\numberline {4.1.2.2}MY\_GETC}{20}{subsubsection.4.1.2.2}%
\contentsline {subsubsection}{\numberline {4.1.2.3}MY\_OPEN}{20}{subsubsection.4.1.2.3}%
\contentsline {subsubsection}{\numberline {4.1.2.4}str\_dup}{20}{subsubsection.4.1.2.4}%
\contentsline {subsection}{\numberline {4.1.3}Documentation des fonctions}{20}{subsection.4.1.3}%
\contentsline {subsubsection}{\numberline {4.1.3.1}detruire\_configuration()}{20}{subsubsection.4.1.3.1}%
\contentsline {subsubsection}{\numberline {4.1.3.2}detruire\_liste\_configuration()}{20}{subsubsection.4.1.3.2}%
\contentsline {subsubsection}{\numberline {4.1.3.3}detruire\_param()}{21}{subsubsection.4.1.3.3}%
\contentsline {subsubsection}{\numberline {4.1.3.4}detruire\_section()}{21}{subsubsection.4.1.3.4}%
\contentsline {subsubsection}{\numberline {4.1.3.5}liste\_parametres\_configuration()}{22}{subsubsection.4.1.3.5}%
\contentsline {subsubsection}{\numberline {4.1.3.6}liste\_sections\_configuration()}{22}{subsubsection.4.1.3.6}%
\contentsline {subsubsection}{\numberline {4.1.3.7}nb\_parametres\_configuration()}{23}{subsubsection.4.1.3.7}%
\contentsline {subsubsection}{\numberline {4.1.3.8}nb\_sections\_configuration()}{23}{subsubsection.4.1.3.8}%
\contentsline {subsubsection}{\numberline {4.1.3.9}parse\_configuration()}{24}{subsubsection.4.1.3.9}%
\contentsline {subsubsection}{\numberline {4.1.3.10}read\_param()}{24}{subsubsection.4.1.3.10}%
\contentsline {subsubsection}{\numberline {4.1.3.11}read\_section()}{25}{subsubsection.4.1.3.11}%
\contentsline {subsubsection}{\numberline {4.1.3.12}read\_string()}{25}{subsubsection.4.1.3.12}%
\contentsline {subsubsection}{\numberline {4.1.3.13}read\_valeurs()}{26}{subsubsection.4.1.3.13}%
\contentsline {subsubsection}{\numberline {4.1.3.14}store\_val()}{26}{subsubsection.4.1.3.14}%
\contentsline {subsubsection}{\numberline {4.1.3.15}valeurs\_configuration()}{26}{subsubsection.4.1.3.15}%
\contentsline {subsubsection}{\numberline {4.1.3.16}valeurs\_configuration\_n()}{27}{subsubsection.4.1.3.16}%
\contentsline {section}{\numberline {4.2}Référence du fichier src/parse\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}config.h}{28}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Description détaillée}{29}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Documentation des macros}{29}{subsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.2.1}DLLAPI}{29}{subsubsection.4.2.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2.2}DLLCALL}{29}{subsubsection.4.2.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Documentation des définitions de type}{29}{subsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.3.1}ERR\_FONCT}{29}{subsubsection.4.2.3.1}%
\contentsline {subsection}{\numberline {4.2.4}Documentation des fonctions}{29}{subsection.4.2.4}%
\contentsline {subsubsection}{\numberline {4.2.4.1}detruire\_configuration()}{29}{subsubsection.4.2.4.1}%
\contentsline {subsubsection}{\numberline {4.2.4.2}detruire\_liste\_configuration()}{30}{subsubsection.4.2.4.2}%
\contentsline {subsubsection}{\numberline {4.2.4.3}liste\_parametres\_configuration()}{30}{subsubsection.4.2.4.3}%
\contentsline {subsubsection}{\numberline {4.2.4.4}liste\_sections\_configuration()}{31}{subsubsection.4.2.4.4}%
\contentsline {subsubsection}{\numberline {4.2.4.5}nb\_parametres\_configuration()}{31}{subsubsection.4.2.4.5}%
\contentsline {subsubsection}{\numberline {4.2.4.6}nb\_sections\_configuration()}{32}{subsubsection.4.2.4.6}%
\contentsline {subsubsection}{\numberline {4.2.4.7}parse\_configuration()}{32}{subsubsection.4.2.4.7}%
\contentsline {subsubsection}{\numberline {4.2.4.8}valeurs\_configuration()}{33}{subsubsection.4.2.4.8}%
\contentsline {subsubsection}{\numberline {4.2.4.9}valeurs\_configuration\_n()}{33}{subsubsection.4.2.4.9}%
\contentsline {section}{\numberline {4.3}parse\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}config.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h}{34}{section.4.3}%
\contentsline {section}{\numberline {4.4}Référence du fichier src/parse\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}configP.h}{35}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Description détaillée}{36}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}Documentation des macros}{37}{subsection.4.4.2}%
\contentsline {subsubsection}{\numberline {4.4.2.1}DPRINT}{37}{subsubsection.4.4.2.1}%
\contentsline {subsubsection}{\numberline {4.4.2.2}EAT\_TO\_EOL}{37}{subsubsection.4.4.2.2}%
\contentsline {subsubsection}{\numberline {4.4.2.3}PARAM\_NAME\_LEN}{37}{subsubsection.4.4.2.3}%
\contentsline {subsubsection}{\numberline {4.4.2.4}PRINT\_ERR}{37}{subsubsection.4.4.2.4}%
\contentsline {subsubsection}{\numberline {4.4.2.5}SECTION\_NAME\_LEN}{38}{subsubsection.4.4.2.5}%
\contentsline {subsubsection}{\numberline {4.4.2.6}VALEUR\_LEN}{38}{subsubsection.4.4.2.6}%
\contentsline {subsection}{\numberline {4.4.3}Documentation des définitions de type}{38}{subsection.4.4.3}%
\contentsline {subsubsection}{\numberline {4.4.3.1}CONF}{38}{subsubsection.4.4.3.1}%
\contentsline {subsubsection}{\numberline {4.4.3.2}MY\_FILE\_t}{38}{subsubsection.4.4.3.2}%
\contentsline {subsubsection}{\numberline {4.4.3.3}PARAM}{38}{subsubsection.4.4.3.3}%
\contentsline {subsubsection}{\numberline {4.4.3.4}SECTION}{38}{subsubsection.4.4.3.4}%
\contentsline {subsection}{\numberline {4.4.4}Documentation des fonctions}{38}{subsection.4.4.4}%
\contentsline {subsubsection}{\numberline {4.4.4.1}detruire\_param()}{39}{subsubsection.4.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.4.2}detruire\_section()}{39}{subsubsection.4.4.4.2}%
\contentsline {subsubsection}{\numberline {4.4.4.3}read\_param()}{39}{subsubsection.4.4.4.3}%
\contentsline {subsubsection}{\numberline {4.4.4.4}read\_section()}{39}{subsubsection.4.4.4.4}%
\contentsline {subsubsection}{\numberline {4.4.4.5}read\_string()}{39}{subsubsection.4.4.4.5}%
\contentsline {subsubsection}{\numberline {4.4.4.6}read\_valeurs()}{40}{subsubsection.4.4.4.6}%
\contentsline {subsubsection}{\numberline {4.4.4.7}store\_val()}{40}{subsubsection.4.4.4.7}%
\contentsline {subsection}{\numberline {4.4.5}Documentation des variables}{40}{subsection.4.4.5}%
\contentsline {subsubsection}{\numberline {4.4.5.1}CONF\_TYPE}{40}{subsubsection.4.4.5.1}%
\contentsline {subsubsection}{\numberline {4.4.5.2}NbErr}{40}{subsubsection.4.4.5.2}%
\contentsline {subsubsection}{\numberline {4.4.5.3}NbLignes}{40}{subsubsection.4.4.5.3}%
\contentsline {section}{\numberline {4.5}parse\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}config\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}P.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h}{41}{section.4.5}%
\contentsline {section}{\numberline {4.6}Référence du fichier src/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Snmp2\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Influx\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}DB.c}{42}{section.4.6}%
\contentsline {subsection}{\numberline {4.6.1}Description détaillée}{43}{subsection.4.6.1}%
\contentsline {subsection}{\numberline {4.6.2}Documentation des définitions de type}{43}{subsection.4.6.2}%
\contentsline {subsubsection}{\numberline {4.6.2.1}\_snmp2influxdb\_config\_t}{43}{subsubsection.4.6.2.1}%
\contentsline {subsubsection}{\numberline {4.6.2.2}\_snmp2influxdb\_oid\_t}{43}{subsubsection.4.6.2.2}%
\contentsline {subsubsection}{\numberline {4.6.2.3}elem\_t}{44}{subsubsection.4.6.2.3}%
\contentsline {subsubsection}{\numberline {4.6.2.4}mesure\_t}{44}{subsubsection.4.6.2.4}%
\contentsline {subsection}{\numberline {4.6.3}Documentation des fonctions}{44}{subsection.4.6.3}%
\contentsline {subsubsection}{\numberline {4.6.3.1}dispatche\_configuration()}{44}{subsubsection.4.6.3.1}%
\contentsline {subsubsection}{\numberline {4.6.3.2}display\_response()}{45}{subsubsection.4.6.3.2}%
\contentsline {subsubsection}{\numberline {4.6.3.3}display\_series()}{45}{subsubsection.4.6.3.3}%
\contentsline {subsubsection}{\numberline {4.6.3.4}do\_term()}{45}{subsubsection.4.6.3.4}%
\contentsline {subsubsection}{\numberline {4.6.3.5}main()}{46}{subsubsection.4.6.3.5}%
\contentsline {subsubsection}{\numberline {4.6.3.6}new\_mesure()}{46}{subsubsection.4.6.3.6}%
\contentsline {subsubsection}{\numberline {4.6.3.7}print\_result()}{46}{subsubsection.4.6.3.7}%
\contentsline {subsubsection}{\numberline {4.6.3.8}store\_fields()}{47}{subsubsection.4.6.3.8}%
\contentsline {subsubsection}{\numberline {4.6.3.9}store\_index()}{47}{subsubsection.4.6.3.9}%
\contentsline {subsubsection}{\numberline {4.6.3.10}store\_tables()}{47}{subsubsection.4.6.3.10}%
\contentsline {subsection}{\numberline {4.6.4}Documentation des variables}{48}{subsection.4.6.4}%
\contentsline {subsubsection}{\numberline {4.6.4.1}Config}{48}{subsubsection.4.6.4.1}%
\contentsline {subsubsection}{\numberline {4.6.4.2}TerminaisonAsked}{48}{subsubsection.4.6.4.2}%
\contentsline {subsubsection}{\numberline {4.6.4.3}Verbose}{48}{subsubsection.4.6.4.3}%
\contentsline {chapter}{Index}{49}{section*.37}%
