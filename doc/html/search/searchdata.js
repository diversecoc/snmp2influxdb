var indexSectionsWithContent =
{
  0: "_acdefilmnoprstuv",
  1: "_mpt",
  2: "ps",
  3: "dlmnprsv",
  4: "acdefinopstuv",
  5: "_cemps",
  6: "ft",
  7: "dempsv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enumvalues",
  7: "defines"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Valeurs énumérées",
  7: "Macros"
};

